# -*- coding: utf-8 -*-

"""

@authors : ACHIM SCHILLING ,PALERESSOMPOULLE DANY , MORÉ SIMON, MORALES GABRIEL

"""
#### GUI for getting Data from Arduino
#### Version 1.0
############################## import  different libraries
import os, sys
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
import matplotlib.image as mpimg
import numpy as np
import serial
from serial import Serial, tools
from serial.tools import list_ports
from PyQt5 import  QtCore
from PyQt5.QtWidgets import QApplication, QMessageBox
from model_dataloger_pression import *
from view_dataloger_pression import *
###################

class Controller:
    def __init__(self):
        ####### initializer variables
        self.view = MainWindow(self)
        self.model = Model(self)
        self.second_window = None # reference for the second window
        self.third_window = None # reference for the third window
        self.table_database_window = None # reference for the databases (patients and experiences)
        #window
        ####### variables that keep track of pression range selected by the user, if its 0, 
        # the selection is automatic by the plot
        self.pressure_range1 = 0 # pression range of canvas AX1 (mainwindow) 
        self.pressure_range2 = 0 # pression range of canvas AX2 (mainwindow) 
        self.pressure_range_thirdwindow = 0 # pression range of canvas AX1 (thirdwindow) 
        ####### variables that keep track of time range selected by the user, the dafault 
        # values is 10 seconds ,with a minimum of 5 seconds
        self.time_window = 10 # time range of canvas AX2 (mainwindow) 
        self.time_window_thirdwindow = 10 # time range of canvas AX1 (thirdwindow)
        ####### variables to keep in the memory the selected R232 Serial ports
        self.current_ports = []
        self.selected_port = []
        self.serial = None # reference for the Serial port connection
        ####### values created for manage the event tags that can mark the ax1 plot (FEATURE 
        # READY BUT NOT DISPLAYED IN THE PROGRAMM).
        self.plot1_markers = np.array([0])
        self.markerbool = False
        self.marker_counter = 0
        ####### boolean variables serve as switch
        self.snapbool = False # Variable for indicate the programm to create a snapshot of 
                              # the current ax2 plot
        self.start_save_bool = False # variable for indicate the programm to start saving the values 
                                     # from the ax1 plot into the saving variables from the model.
        self.running = False # variable that indicates the programm that the user start reading and 
                             #displaying the data from the Arduino device
        ####### Dictionaries that keeps the specifications for creating the plots of the the main 
        # window and the second window with the snapshot captures
        self.snap_canvas_number = {"ax3": [False,7,4,(17,18),None,None] ,"ax4":[False,7,4,(19,20),None,None], 
                                    "ax5":[False,7,4,(21,22),None,None], "ax6":[False,7,4,(23,24),None,None], 
                                    "ax7":[False,7,4,(25,26),None,None], "ax8":[False,7,4,(27,28),None,None]}
                                #FORMAT {"name of the plot": [mainwindow plot status,grid height, grid long,
                                # (grid position),plot object, data for plot(p1_values_copy,p1_values_copy,
                                # time_window_copy, plot's title)]

        self.snap_canvas_number_second_window = {0:["ax3", False, None] ,
                                                1:["ax4", False, None], 
                                                2:["ax5",False, None], 
                                                3:["ax6",False, None],
                                                4:["ax7",False, None],
                                                5:["ax8",False, None]}
                                                #FORMAT {"plot index": [name of the plot,second window plot 
                                                # status, data for plot]

        ####### The timer function calls every 100 msec the measurement rep function -> read values and update plot
        self.timer = QtCore.QTimer() # Setup a timer to trigger the redraw by calling update_plot.
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.measurement_rep)
        self.timer.start()

        ####### Start port verification procedure, to see if the pressure capture device is connected
        self.port_verification()

        ####### Ask the users if he want to load all the patients information, or start a new set of recordings
        users_response = self.view.data_load_question_view()
        self.data_load_question_controller(users_response)
    
    ####### Serial function for Port verification  
    def port_verification(self):
        """CALL THE PORT NUMBER SELECTED BY THE USER AND CREATES A SERIAL OBJECT TO CONNECT THE PROGRAM WITH 
        THE CAPTURE DEVICE"""
        while True:
            try:
                self.verify_current_ports()
                self.serial = Serial(self.selected_port, baudrate=115200)
                self.serial.reset_input_buffer()
                self.enable_buttons(self.view.widget.startbutton)
                return False #stops the while loop, when a valid option is selected in 
                             #self.view.users_port_selection function
            except  serial.serialutil.SerialException:
                return

    ####### Return the ports list and recovers the users port selection
    def verify_current_ports(self):
        """  it sends a Pop up with al the available ports and ask the user for his selection of the 
        Arduino device, IT DOESNT MANAGE THE BLUETOOH PORTS ( AVAILABLE FOR NEXT DEVELOPMENT in case of
        a bluetooh device use) """
        ports = serial.tools.list_ports.comports() #returns a list will all the available and in use ports
        self.current_ports = []
        for port, desc, hwid in sorted(ports):
            self.current_ports.append([port,desc,hwid])
        if len(self.current_ports) > 0: #this statement discriminates if the user enter an option, but still
            # doesnt discriminate which option (when the bluetooh is available, it shows the 2 ports), because,
            # the name of the device its only showed when the Arduino software its installed.    
            items = []
            for i in self.current_ports:
                items.append(i[1])
            item, ok = self.view.users_port_selection(items)
            if ok == False: # This statement indicates if the user leave the QInputDialog without 
                            # entering any option 
                sys.exit()
                # self.verify_current_ports()  : IN CASE YOU NEED TO RETURN TO THE QUESTION
                # if re.search("Arduino Uno", desc): IN CASE WE NEED TO DISCRIMINATE THE USB
                # OR THE BLUETOOH ENTRIES
            for j in self.current_ports:
                if item == j[1]:
                    self.selected_port = j[0] # it stablish the selected port by the user.
                    break
        else:
            self.view.error_message(self, "no device")
            sys.exit()

    ####### Process the response of the user and loads or creates a database accordingly
    def data_load_question_controller(self,users_response):
        if users_response == QMessageBox.Ok:
            database_name = self.view.open_file_name_dialog_mainwindow() # opens a dialog windows for the
                                                                         # user to choose a database file
            name, extension = os.path.splitext(database_name)
            if extension == ".db":
                self.model.create_connection(database_name)
                database_patients_id_list = self.model.return_all_patients_id()
                if len(database_patients_id_list) != 0:
                    self.enable_buttons(self.view.widget.next_patient_button,
                                        self.view.widget.previous_patient_button)
            elif database_name == ":memory:":
                self.model.create_connection(":memory:")
            else: # discriminates if the entered database is a .db file, and if not, launches a ERROR 
                  # message from the GUI, and relaunches the function itself.
                self.view.error_message(self,"no database")
                self.data_load_question_controller(QMessageBox.Ok)
                return
        elif users_response == QMessageBox.No:
            database_name, ok = self.view.new_database_creation_view() # funtion that launches a input 
                                                                       # interface for the user to enter 
                                                                       # a new for the new database
            if ok:
                database_name = database_name + ".db"
                self.model.create_connection(database_name)
            else:
                self.model.create_connection(":memory:")


    ####### functions connected to buttons
        
    def start(self):
        """ This function, changes the self.running variable to True, that allows the beggining
        of the data saving (into a variable to be shown into the graph of the GUI, just for data 
        display porpouses, nor recording for exportation or the database) by the model through 
        the interaction of the functions def CONTROLLER-measurement_rep ---> 
        MODEL-complete_measurement_repetition().It also manages the buttons on the GUI that are
         going to be activated or deactivated once the programm is running and saving data"""
        self.running = True
        self.disable_buttons(self.view.widget.startbutton)
        self.enable_buttons(self.view.widget.stopbutton,self.view.widget.snapbutton,
                            self.view.widget.startsavebutton,self.view.widget.resetbutton, 
                            self.view.widget.auto_pressure_button_ax1, self.view.widget.doubleSpinBox_pressure1,
                            self.view.widget.default_time_button,self.view.widget.auto_pressure_button_ax2,
                            self.view.widget.doubleSpinBox_pressure2,self.view.widget.doubleSpinBox_time2, 
                            self.view.scroll_bar, self.view.widget.combobox_time, self.view.widget.markerbutton)
        self.selected_port = []

    def stop(self):
        """ This function, changes the self.running variable to False, stoping the data recording
        by the model through the interaction of the functions def CONTROLLER-measurement_rep
        ---> MODEL-complete_measurement_repetition().
        It also manages the buttons on the GUI that are going to be activated or deactivated once 
        the programm stops saving data"""
        self.running = False
        self.model.init_count = 0
        self.enable_buttons(self.view.widget.startbutton)
        self.disable_buttons(self.view.widget.stopbutton,self.view.widget.snapbutton, 
                            self.view.widget.startsavebutton, self.view.widget.stopsavebutton,
                            self.view.widget.doubleSpinBox_pressure1,self.view.widget.auto_pressure_button_ax1,
                            self.view.widget.doubleSpinBox_pressure2,self.view.widget.auto_pressure_button_ax2,
                            self.view.widget.doubleSpinBox_time2 ,self.view.widget.default_time_button, 
                            self.view.widget.combobox_time, self.view.widget.markerbutton)

    def snap(self):
        """ This funtion changes the status of self.snapbool to True, allowing the saving of all 
        the data currently presented in the ax2 plot, and displays it in the GUI at the minisnaps 
        plot area with the current pressure anda time range selected by the user, by calling the 
        function self.view.snapshot()"""
        self.snapbool =  True
        self.view.snapshot(self.pressure_range2, self.time_window)
        
    def start_saving(self):
        """ This funtion changes the status of self.start_save_bool to True, allowing the save of 
        the data currently presented in ax1 plot to be stored in a variable that will eventually 
        be exported into one of the selected file format and to be saved into the database.It also
        manages the buttons on the GUI that are going to be activated or deactivated once the 
        programm is running and saving data"""
        self.start_save_bool  = True
        self.enable_buttons(self.view.widget.stopsavebutton)
        self.disable_buttons(self.view.widget.startsavebutton)
        
    def stop_saving(self):
        """ This funtion changes the status of self.start_save_bool to False, stoping the saving 
        of the data in ax1 plot, displaying to the user the selection of the filetype, filename 
        and location of the new file. and finally, calling the function self.model.export_data_model
        to create he new file with the selected data. It also, automatically save the recorded
        experience data, along with the patients information into the database structure (patient
        table, experience table, and experience ID specifi table)
        It also manages the buttons on the GUI that are going to be activated or deactivated once 
        the programm has stop the recording"""
        self.start_save_bool  = False
        self.enable_buttons(self.view.widget.startsavebutton)
        self.disable_buttons(self.view.widget.stopsavebutton)
        self.stop()
        try:
            self.model.add_new_experience_database_model(tuple([int(self.view.widget.patient_id_label.text())])) 
            # insert into the database
            self.model.create_table_experience_data() 
            # create a table with the name of the experience ID to keep the graph data      
        except ValueError: 
            pass
        selected_file_type = self.view.saving_file_type_view()
        if selected_file_type is not None:
            fileName = self.view.save_filedialog_mainwindow()
            if fileName is not None:
                pass
            else:
                self.model.values_save_p1_data_file = []
                self.model.values_save_p2_data_file = [] 
                return
        else:
            self.model.values_save_p1_data_file = []
            self.model.values_save_p2_data_file = [] 
            return
        self.model.export_data_model(fileName ,selected_file_type)

    def reset_canvas(self):
        """ this function restores all the values stored for creating the plots, 
        clearing all the plot and snapshot to reinitiate another recording session 
        with the same patient (it doesnt change the current patient)"""
        self.model.values_save_p1 = [] # to save the values p1
        self.model.values_save_p2 = [] # to save the values p2
        self.plot1_markers = np.array([0])
        self.stop()
        self.disable_buttons(self.view.widget.snapbutton, self.view.scroll_bar,
                            self.view.widget.combobox_time, self.view.widget.markerbutton,
                            self.view.widget.doubleSpinBox_pressure1,self.view.widget.auto_pressure_button_ax1,
                            self.view.widget.doubleSpinBox_pressure2,self.view.widget.auto_pressure_button_ax2,
                            self.view.widget.doubleSpinBox_time2,self.view.widget.default_time_button)
        self.view.scroll_bar.setValue(99)
        self.view.clear_canvas_view()
        self.disable_all_snap_buttons()
        self.clear_plot_data_second_window()
        self.clear_plots_data_mainwindow_controller()
        
    ######################### Disable the specified buttons 
    def disable_buttons(self, *args):
        """ This function disables the entered buttons objects from the GUI or controller"""
        for entry in args:
           entry.setEnabled(False)

    ######################### Enable the specified buttons 
    def enable_buttons(self, *args):
        """ This function enables the entered buttons objects from the GUI or controller"""
        for entry in args:
           entry.setEnabled(True)
        
    ####### Functions related to canvas and plot manipulation
    def auto_range_canvas_pressure(self):
        """This function discriminates which auto_pressure_button from the GUI was pressed 
        returning the value of self.pressure_range to 0 for the Y axis of the AX1 and AX2 
        plot of the mainwindow, and the AX1 of the third window """
        sender = self.view.sender() # it determines from which object this function was called
        if sender == self.view.widget.auto_pressure_button_ax1:
            self.pressure_range1 = 0
            self.view.widget.doubleSpinBox_pressure1.setValue(0.0)
        elif sender ==  self.view.widget.auto_pressure_button_ax2:
            self.pressure_range2 = 0
            self.view.widget.doubleSpinBox_pressure2.setValue(0.0)
        else:
            self.pressure_range_thirdwindow = 0
            self.third_window.widget.doubleSpinBox_pressure1.setValue(0.0)
            self.third_window.refresh_plot()

    def default_range_canvas_time(self):
        """This function  returns the value of self.time_window to 10 for the X axis of the AX2
        plot of the mainwindow, and the AX1 of the third window """
        sender = self.view.sender() # it determines from which object this function was called
        if sender == self.view.widget.default_time_button:
            self.time_window = 10
            self.view.widget.doubleSpinBox_time2.setValue(10.0)
        else:
            self.time_window_thirdwindow = 10
            self.third_window.widget.doubleSpinBox_time2.setValue(10.0)
            self.third_window.refresh_plot()


    def recover_result_pressure_spinbox(self):
        """this function discrinates if one of the pressure spinbox values has changed
        and in that case, sets the corresponding self.pressure_range to the value entered
        by the user, changing the Y axis of the AX1 and AX2 plot of the mainwindow, and 
        the AX1 of the third window """
        sender = self.view.sender() # it determines from which object this function was called
        if sender == self.view.widget.doubleSpinBox_pressure1:
            value = self.view.widget.doubleSpinBox_pressure1.value()
            self.pressure_range1 = value
        elif sender == self.view.widget.doubleSpinBox_pressure2:
            value = self.view.widget.doubleSpinBox_pressure2.value()
            self.pressure_range2 = value
        else:
           value =  self.third_window.widget.doubleSpinBox_pressure1.value()
           self.pressure_range_thirdwindow = value
           self.third_window.refresh_plot()

    def recover_result_time_spinbox(self):
        """this function discrinates if the value of the time spinbox has changed, and in
        that case, sets the corresponding  self.time_window to the value entered by the 
        user , changing the X axis of the AX2 plot of the mainwindow, and the AX1 of the 
        third window (for the ax1 plot of the second window, the number 10 doesnst match
        the seconds in the X axis, put it serves to augment or diminish the size of 
        the range the same way). Because self.model.values_save_p1_ax2 and self.model.values_save_p2_ax2
         correspond to a zeros list that its filled from the end, in case that the size of 
        self.time_window surpases the number of values in the display, it fill with 0 from the 
        beggining of the list to allow the data plot (same number values in X and y AXIS)"""
        sender = self.view.sender() # it determines from which object this function was called
        if sender == self.view.widget.doubleSpinBox_time2:
            value = self.view.widget.doubleSpinBox_time2.value()
            self.time_window = int(value)
            while len(self.model.values_save_p1_ax2) != (170* value):
                if len(self.model.values_save_p1_ax2) < (170* value):
                    self.model.values_save_p1_ax2.insert(0,0)
                else:
                    self.model.values_save_p1_ax2.pop(0)
            while len(self.model.values_save_p2_ax2) != (170* value):
                if len(self.model.values_save_p2_ax2) < (170* value):
                    self.model.values_save_p2_ax2.insert(0,0)
                else:
                    self.model.values_save_p2_ax2.pop(0)
        else:
            value = self.third_window.widget.doubleSpinBox_time2.value()
            self.time_window_thirdwindow = value
            self.third_window.refresh_plot()
    
    def select_snaps_controller(self, plot_ax):
        """This Function its triggered every time the values in one of the Check box 
        of the Snaps manager from the mainwindow changes. It evaluates if the 
        check box its check or uncheck, and it changes the status of the plot in the 
        self.snap_canvas_number_second_window dictionary, as long with data for 
        plottinf the graphs"""
        sender = self.view.sender()
        for i, j in self.snap_canvas_number_second_window.items():
            if j[0] == plot_ax:
                if sender.isChecked() == True:
                    self.snap_canvas_number_second_window[i][1] = True 
                    # pass the plot status as TRUE, which means that is open in the second window
                    self.snap_canvas_number_second_window[i][2] = self.snap_canvas_number[plot_ax][5] 
                    # it passes the plotting data from the the clicked  plot object to 
                    # self.snap_canvas_number_second_window
                    return
                else:
                    self.snap_canvas_number_second_window[i][1] = False 
                    # pass the plot status as TRUE, which means that is open in the second window
                    self.snap_canvas_number_second_window[i][2] = None 
                    # it passes the plotting data from the the clicked  plot object to 
                    # self.snap_canvas_number_second_window
                    return

    ####### Functions related to snapshot creation and snapshot display i the second window.
    # It also contains the functions to save the snapshots
    def open_selected_snaps(self):
        """Function triggered by the self.view.open_snaps button, which calls for opening
        of the second window displaying a larger version of the selected mini snaps """
        self.open_second_window()

    
    def enable_disable_snap_button(self):
        """This function discriminates if al the checkboxes are disabled. If thats the 
        case, it disable the self.view.widget.open_snaps button.
        In case there is al least one enabled checkbox, the self.view.widget.open_snaps
        stays active """
        checkbox_status_list = []
        for i in self.view.snaps_buttons_dico_mainwindow.values():
            if i[0] == 'Open Snaps':
                pass
            else:
                checkbox_status_list.append(i[0])
        if all(i == False for i in checkbox_status_list):
            self.disable_buttons( self.view.widget.open_snaps)
        else:
            self.enable_buttons( self.view.widget.open_snaps)

    def disable_all_snap_buttons(self):
        """ Function that disable all the widgets of the snaps manager box from the mainwindow. 
        This functions its triggered every time the current patients changes or the reset 
        button its pressed  """
        for i in self.view.snaps_buttons_dico_mainwindow.values():
            i[0] = False
            i[1].setChecked(False)
            self.disable_buttons(i[1])
            self.enable_disable_snap_button()

    def snap_dico_checking(self,dico):
        """This function evaluates if all the 6 positions for the snapshots (ax3, ax4,
        ax5, ax6, ax7, ax8 plots from  GUI) are occupied.The index 0 (BOOLEAN) of each 
        plot object from self.snap_canvas_number indicates if the ax is currently in display.
        when all the positions are filled (all the boleans are TRUE), it changes all the 
        boleans to FALSE, that produces that the new snapshot are going to start using 
        the positions of the olds snapshots plots  """

        values = list(dico.values())
        check_list = []
        for i in values:
            check_list.append(i[0])
        if all(i == True for i in check_list):
            for i in self.snap_canvas_number.keys():
                self.snap_canvas_number[i][0] = False


    def manage_canvas_click(self, event):
        """This function activates every time that the mainwindow canvas is clicked. 
        --> When the AX1 plot of the mainwindow it pressed with the right click, 
        it set all the values of the AX1 and AX2 to 0, emptying both plots. It 
        doesnt changes the current patients, allowing the user to refresh the graphs
         of the same recording session.
        --> When the AX2 plot of the mainwindow it pressed with the right click, it
        set all the values of  AX2 to 0, emptying the plots.
        It doesnt changes the current patients, allowing the user to refresh the graphs 
        of the same recording session
        -->When one of the click is on the mini snaps with the left button, it opens a 
        input dialog box, that allow the user to change the pressed mini snap titles.
        -->If the snapshot is clicked with the right button, it closes the snap and
         the current second window."""

        if event.inaxes == self.view.screen_canvas.ax1 and event.button == 3:   
            # THE AX1 PLOT ITS CLICKED WITH THE RIGHT CLICK
            self.clear_plots_data_mainwindow_controller()
            return

        elif event.inaxes == self.view.screen_canvas.ax2 and event.button == 3: 
            # THE AX2 PLOT ITS CLICKED WITH THE RIGHT CLICK
            self.model.values_save_p1_ax2 = [0] * 170 * self.time_window
            self.model.values_save_p2_ax2 = [0] * 170 * self.time_window
            self.view.refresh_plot_view_mainwindow(self.pressure_range1,self.pressure_range2, self.time_window)
            return

        active_canvas = [] # CREATES A LIST WITH ALL THE CURRENT ACTIVE MINI SNAPS
        for i in self.snap_canvas_number.values():
                if i[4] == None:
                    pass
                else:
                    active_canvas.append(i[4])

        for j,i in enumerate(active_canvas):
            if event.inaxes == i and event.button == 1 : # THE SNAPSHOT IMAGE IS CLICKED WITH THE LEFT CLICK 
                snap_title, ok = self.view.set_snap_title_view()
                if ok:
                    self.snap_canvas_number[self.snap_canvas_number_second_window[j][0]][4].title.set_text(snap_title) 
                    # Changes the title of the minisnap of the mainwindow
                    self.snap_canvas_number[self.snap_canvas_number_second_window[j][0]][5][3] = snap_title 
                    # PASSES THE VARIABLE FOT HE TITLE TO BE ADDED TO THE GRAPH IN THE SECOND WINDOW
                else:
                    return
            elif event.inaxes == i and event.button == 3 : 
                #THE SNAPSHOT IMAGE IS CLICKED WITH THE RIGHT CLICK
                if self.second_window is not None:
                    self.second_window.close()
                self.snap_canvas_number_second_window[j][1] = False
                self.snap_canvas_number_second_window[j][2] = None
                self.snap_canvas_number[self.snap_canvas_number_second_window[j][0]][4].remove()
                self.snap_canvas_number[self.snap_canvas_number_second_window[j][0]][4] = None
                self.snap_canvas_number[self.snap_canvas_number_second_window[j][0]][5] = None                
                self.snap_canvas_number[self.snap_canvas_number_second_window[j][0]][0] = False
                self.view.screen_canvas.draw()
                #### this lines disable the buttons in the snaps manager, not allowing to be selected
                self.view.snaps_buttons_dico_mainwindow[self.snap_canvas_number_second_window[j][0]][1].setChecked(False)
                self.view.snaps_buttons_dico_mainwindow[self.snap_canvas_number_second_window[j][0]][0] = False
                self.disable_buttons(self.view.snaps_buttons_dico_mainwindow[self.snap_canvas_number_second_window[j][0]][1])
                self.enable_disable_snap_button()

    def clear_plots_data_mainwindow_controller(self):
        """ function thats set all the values of the recording session to base values"""
        self.model.values_save_p1 = [] 
        self.model.values_save_p2 = [] 
        self.model.values_save_p1_ax2 = [0] * 170 * self.time_window
        self.model.values_save_p2_ax2 = [0] * 170 * self.time_window
        self.plot1_markers = np.array([0])
        self.view.refresh_plot_view_mainwindow(self.pressure_range1,self.pressure_range2, self.time_window)

    def clear_plot_data_second_window(self):
        """ function that set to base values all the plots dictionaries"""
        self.snap_canvas_number = {"ax3": [False,7,4,(17,18),None,None],"ax4":[False,7,4,(19,20),None,None],
                                    "ax5":[False,7,4,(21,22),None,None],"ax6":[False,7,4,(23,24),None,None], 
                                    "ax7":[False,7,4,(25,26),None,None],"ax8":[False,7,4,(27,28),None,None]
                                    }
        self.snap_canvas_number_second_window = {0:["ax3", False, None],1:["ax4", False, None], 
                                                2:["ax5",False, None],3:["ax6",False, None],
                                                4:["ax7",False, None], 5:["ax8",False, None]
                                                }

    def delete_subplot_secondwindow_controller(self, plot):
        """ it receives the plot object reference from the GUI, and compares the
        list of actives plot of the second window (self.snap_canvas_number_second_window) when it 
        founds it, it changes the status (index 1) to FALSE and deletes sets the objects index 
        (index 2) as NONE, afeter that, it closes the second window and opens a new one """
        for k,i in self.snap_canvas_number_second_window.items():
            if i[0] == plot:
                self.snap_canvas_number_second_window[k][1] = False
                self.snap_canvas_number_second_window[k][2] = None
                self.second_window.close()
                self.second_window = SecondWindow(self) 
                # it closes the window and opens a new one because, it also need to update to buttons 
                # associated with each plot
                self.second_window.show()

    def save_subplot_secondwindow_controller(self, view, plot):
        """ Creates a JPG image of the selected snapshot of the second window """
        selected_file_type = self.second_window.saving_file_type_view()
        if selected_file_type == None:
            return
        elif selected_file_type == ".csv":
            filename = self.second_window.saveFileDialog_secondwindow()
            if filename == None:
                return
            for i in self.snap_canvas_number_second_window.values():
                if i[0] == plot:
                    self.model.export_data_model_second_window(filename,i[2],None)
        

        elif selected_file_type == ".jpg":
            extent = getattr(self.second_window.screen_canvas2.fig ,plot).get_window_extent().transformed(
                self.second_window.screen_canvas2.fig.dpi_scale_trans.inverted())
            filename = self.second_window.saveFileDialog_secondwindow()
            if filename == None:
                return 
            self.second_window.screen_canvas2.fig.savefig(filename, bbox_inches = extent.expanded(1.1, 1.8))

    def create_report_secondwindow_controller(self):   
        """ Function triggered by the self.widget.export_button of the second window. It 
        creates images of each one of the in display graphs and creates a 
        basic report that includes, the graphs, patients information ('Doctors
        name, Patients name, Patients ID, Date and City), and gives the user to insert
        observations to the report"""
        Header = "Patient's Report"
        Contact = 'Doctors name:`{doc}` \nPatients name:`{pat}` \nPatients ID:`{id}`\nDate:`{dat}`\nCity:`{cit}`'.format(
            doc = self.model.current_patient[0],pat =self.model.current_patient[1] ,
            id=self.model.current_patient[2],dat =self.model.current_patient[3] ,
            cit = self.model.current_patient[4])
        page = 'Page 1'
        observations, ok  = self.second_window.observation_input_dialog_secondwindow() 
        #opens a dialog window that ask the user if he wants to add observation to the report
        if ok == False:
            observations = ""
        else:
            tab = round(len(observations)/70)
            print(tab)
            if tab > 0: # this part of the code insert the  "\n" charachters in case the observations
                        # added by the user exced the 70 charachtes lenght. This improves visualization 
                        # in the report
                for i in range(1,tab+1):
                    observations = list(observations).copy()
                    observations.insert(i * 70, "\n")
                    observations = ''.join(observations)
            else:
                pass
        # set font
        plt.rcParams['font.family'] = 'sans-serif'
        plt.rcParams['font.sans-serif'] = 'STIXGeneral'
        # set A4 paper
        fig, ax = plt.subplots(figsize=(8.5, 11))
        # set background color
        ax.set_facecolor('white')
        # remove axes
        plt.axis('off')
        snap_count = 0
        count = 0
        for i in self.snap_canvas_number_second_window.values():
            if i[1] == True:
                snap_count += 1
        # add text
        plt.annotate(Header, (.30,0.99), weight='regular', fontsize=20, alpha=.6 )
        plt.annotate(Contact, (.70,0.95), weight='bold', fontsize=14,alpha=.6)
        plt.annotate(page, (.48,.01), weight='medium', fontsize=10)
        # because the size of the plot changes with the number of plots displayed in the window, the values of the position
        # of the elementos in the report must be specific for each plot number. 
        if snap_count == 6: 
            plt.annotate("OBSERVATIONS: " + observations, (.05,.15), weight='medium', fontsize=10)
        elif snap_count == 5:
            plt.annotate("OBSERVATIONS: " + observations, (.05,.15), weight='medium', fontsize=10)
        elif snap_count == 4:
            plt.annotate("OBSERVATIONS: " + observations, (.05,.15), weight='medium', fontsize=10)
        elif snap_count == 3:
            plt.annotate("OBSERVATIONS: " + observations, (.05,.20), weight='medium', fontsize=10)
        elif snap_count == 2:
            plt.annotate("OBSERVATIONS: " + observations, (.05,.30), weight='medium', fontsize=10)
        elif snap_count == 1:
            plt.annotate("OBSERVATIONS: " + observations, (.05,.50), weight='medium', fontsize=10)
        graphs_coord_1_plot = [(0.48, 0.75)]
        graphs_coord_2_plots = [(0.48, 0.75),(0.48, 0.51)]
        graphs_coord_3_plots = [(0.48, 0.80),(0.48, 0.60),(0.48, 0.40)]
        graphs_coord_4_plots = [(0.48, 0.80),(0.48, 0.65),(0.48, 0.47),(0.48, 0.29)]
        graphs_coord_5_plots = [(0.48, 0.80),(0.48, 0.68),(0.48, 0.56),(0.48, 0.44),(0.48, 0.32)]
        graphs_coord_6_plots = [(0.48, 0.80),(0.48, 0.68),(0.48, 0.56),(0.48, 0.44),(0.48, 0.32), (0.48, 0.20)]
        if snap_count == 6:
            for i in self.snap_canvas_number_second_window.values():
                if i[1] == True: 
                    extent = getattr(self.second_window.screen_canvas2.fig ,i[0]).get_window_extent().transformed(self.second_window.screen_canvas2.fig.dpi_scale_trans.inverted())
                    self.second_window.screen_canvas2.fig.savefig(i[0] + ".png", bbox_inches = extent.expanded(1.1, 1.8))
                    first_plot = mpimg.imread(i[0] + ".png")
                    os.remove(i[0] + ".png")
                    imagebox1 = OffsetImage(first_plot, zoom=0.4)
                    a = AnnotationBbox(imagebox1, graphs_coord_6_plots[count],frameon=False)
                    ax.add_artist(a)
                    count += 1
        elif snap_count == 5:
            for i in self.snap_canvas_number_second_window.values():
                if i[1] == True: 
                    extent = getattr(self.second_window.screen_canvas2.fig ,i[0]).get_window_extent().transformed(self.second_window.screen_canvas2.fig.dpi_scale_trans.inverted())
                    self.second_window.screen_canvas2.fig.savefig(i[0] + ".png", bbox_inches = extent.expanded(1.1, 1.75))
                    first_plot = mpimg.imread(i[0] + ".png")
                    os.remove(i[0] + ".png")
                    imagebox1 = OffsetImage(first_plot, zoom=0.4)
                    a = AnnotationBbox(imagebox1, graphs_coord_5_plots[count],frameon=False)
                    ax.add_artist(a)
                    count += 1
        elif snap_count == 4:
            for i in self.snap_canvas_number_second_window.values():
                if i[1] == True: 
                    extent = getattr(self.second_window.screen_canvas2.fig ,i[0]).get_window_extent().transformed(self.second_window.screen_canvas2.fig.dpi_scale_trans.inverted())
                    self.second_window.screen_canvas2.fig.savefig(i[0] + ".png", bbox_inches = extent.expanded(1.1, 1.52))
                    first_plot = mpimg.imread(i[0] + ".png")
                    os.remove(i[0] + ".png")
                    imagebox1 = OffsetImage(first_plot, zoom=0.4)
                    a = AnnotationBbox(imagebox1, graphs_coord_4_plots[count],frameon=False)
                    ax.add_artist(a)
                    count += 1
        elif snap_count == 3:
            for i in self.snap_canvas_number_second_window.values():
                if i[1] == True: 
                    extent = getattr(self.second_window.screen_canvas2.fig ,i[0]).get_window_extent().transformed(self.second_window.screen_canvas2.fig.dpi_scale_trans.inverted())
                    self.second_window.screen_canvas2.fig.savefig(i[0] + ".png", bbox_inches = extent.expanded(1.1, 1.43))
                    first_plot = mpimg.imread(i[0] + ".png")
                    os.remove(i[0] + ".png")
                    imagebox1 = OffsetImage(first_plot, zoom=0.4)
                    a = AnnotationBbox(imagebox1, graphs_coord_3_plots[count],frameon=False)
                    ax.add_artist(a)
                    count += 1
        elif snap_count == 2:
            for i in self.snap_canvas_number_second_window.values():
                if i[1] == True: 
                    extent = getattr(self.second_window.screen_canvas2.fig ,i[0]).get_window_extent().transformed(self.second_window.screen_canvas2.fig.dpi_scale_trans.inverted())
                    self.second_window.screen_canvas2.fig.savefig(i[0] + ".png", bbox_inches = extent.expanded(1.1, 1.325))
                    first_plot = mpimg.imread(i[0] + ".png")
                    os.remove(i[0] + ".png")
                    imagebox1 = OffsetImage(first_plot, zoom=0.4)
                    a = AnnotationBbox(imagebox1, graphs_coord_2_plots[count],frameon=False)
                    ax.add_artist(a)
                    count += 1
        elif snap_count == 1:
            for i in self.snap_canvas_number_second_window.values():
                if i[1] == True: 
                    extent = getattr(self.second_window.screen_canvas2.fig ,i[0]).get_window_extent().transformed(self.second_window.screen_canvas2.fig.dpi_scale_trans.inverted())
                    self.second_window.screen_canvas2.fig.savefig(i[0] + ".png", bbox_inches = extent.expanded(1.1, 1.70))
                    first_plot = mpimg.imread(i[0] + ".png")
                    os.remove(i[0] + ".png")
                    imagebox1 = OffsetImage(first_plot, zoom=0.4)
                    a = AnnotationBbox(imagebox1, graphs_coord_1_plot[count],frameon=False)
                    ax.add_artist(a)
                    count += 1
        filename = self.second_window.saveFileDialog_secondwindow()
        if filename == None:
            filename = "patients Report" 
        plt.savefig(filename  + ".pdf", dpi=300, bbox_inches='tight')
    
    def export_data_secondwindow_controller(self):
        filename = self.second_window.saveFileDialog_secondwindow()
        if filename == None:
            return
        export_dico = {}
        for i in self.snap_canvas_number_second_window.values():
            if i[1] == True:
                export_dico[i[0]+"_P1"] = i[2][0]
                export_dico[i[0]+"_P2"] = i[2][1]
        self.model.export_data_model_second_window(filename,None,export_dico)

    def open_second_window(self):
        """ Function for creating the secondwindow object, and displaying it on 
        the screen, if there is another second window already open,it closes it 
        and creates an new one"""
        if self.second_window:
             self.second_window.close()
        self.second_window = SecondWindow(self)
        self.second_window.show()
    
    ####### Functions related to the data adcquisition and saving from the Arduino pressure device 
    def measurement_rep(self):
        """start a measurement (This function its been call every 100 msec by the self.timer 
        variable, and calls the function in the object view for display the new adcquire data
        into the screen"""
        if self.running:
            self.model.complete_measurement_repetition()
            self.view.refresh_plot_view_mainwindow(self.pressure_range1,self.pressure_range2, self.time_window)


    def start_marking(self):
        """ This function change the status of the self.markerbool variable, that indicates that the
        new points in the ax1 plot are going to be marked with a different point style """
        self.markerbool = True

    def markers_on_plot(self):
        """ this  functions creates the indexes where the dots in the ax1  plot will be marked 
        and stores it in self.plot1_markers variable, which is used in the MARKEVERY parameter
        of matplotlib plot creation """
        event_duration = self.view.widget.combobox_time.currentText()
        if self.markerbool == True and event_duration != "0":
            self.plot1_markers = np.append(self.plot1_markers,(np.arange(len(self.model.values_save_p1)-95,len(self.model.values_save_p1)-10)))
            self.marker_counter += 1
        if self.marker_counter >= (int(event_duration)*3): # TODO, change this option of a link with a QTimer object, to be more precise, now its working with number of cicles of the measurement_rep function 
            self.markerbool = False
            self.marker_counter = 0

    ####### Functions related to the patients database manipulation 
    def add_new_patient_controller(self):
        """ Function to link the add_patients button in the GUI with the actions in the 
        GUI itself, in waiting fot the users  resposne to be processed by the controller
        fucntion add_new_patient_done_controller  """
        self.stop()
        self.view.add_new_patient_view()
    
    def add_new_patient_done_controller(self):
        """ This function recoverst all the data entered by the user in the QLineEdit 
        widgets of the fields "doctor_name_line_edit", "patient_name_line_edit",
        "city_label_line_edit", and the QDateEdit widget of the field date_line_ediT,
        WHEN THE ADD_PATIENT_BUTTON IS PRESSED .
        In the special case of "patient_id_line_edit", this fields is not modifiable
        by the user, and is automatically asigned by the database  """
        new_patient = []
        for j, i in self.view.edit_fields_objects.items(): 
            # we recover the value of the input objects of the GUI
            if len(i.text()) == 0:
                new_patient.append(None)
            else:
                new_patient.append(i.text())
        if None in new_patient: 
            # in order to create a adecuate format for the SQLITE database input,
            # None values are intercepted before launching the insetion request to the databse 
            self.view.error_message(self, "new entry empty field(s)")
            return
        else:
            
            new_patient.pop(2)
            new_patient = tuple(new_patient)
            # we create a tuple for the SQLITE request
            self.model.add_new_patient_model(new_patient) 
            # send the request to the model to add the new patient
            self.view.add_new_patient_done_view()
            # perform the visual changes in the GUI.
            new_patient = list(new_patient)
            new_patient.insert(2, str(self.model.cursor.lastrowid))
            self.model.current_patient = new_patient.copy()
            self.view.clear_canvas_view()
            self.disable_all_snap_buttons()
            self.clear_plot_data_second_window()
            self.clear_plots_data_mainwindow_controller()
            

    def update_patient_done_controller(self):
        """This function recoverst all the data entered by the user in the 
        QLineEdit widgets of the fields "doctor_name_line_edit", "patient_name_line_edit",
        "city_label_line_edit", and the QDateEdit widget ofthe field date_line_ediT,
         WHEN THE EDIT_PATIENT_BUTTON IS PRESSED .  """
        update_patient = []
        for j, i in self.view.edit_fields_objects.items():
            if len(i.text()) == 0:
                update_patient.append(None)
            else:
                update_patient.append(i.text())
        if None in update_patient:
            self.view.error_message(self, "new entry empty field(s)")
            return
        else:
            self.model.current_patient = update_patient.copy()
            patients_id = update_patient.pop(2) 
            # THis 2 lines put the patient ID at the end of the tuple, for match with 
            # the data base request format
            update_patient.append(patients_id)
            update_patient = tuple(update_patient)
            self.model.update_patient_model(update_patient)
            
            self.view.update_patient_done_view() 

    def update_patient_controller(self):
        """ Function to link the Edit_patients button in the GUI with the actions 
        in the GUI itself, in waiting fot the users  resposne to be processed by the 
        controller fucntion update_patient_done_controller  """
        self.stop()    
        self.view.update_patient_view()
    
    def cancel_action_controller(self):
        """ This function calls the function in the view, in case the user regrets 
        to perform changes into the database"""
        self.view.cancel_action_view()

    def delete_patient_controller(self):
        """ Function for the delete_patient_button, that calls for an input 
        dialog box in the GUI for retrieve the users reposponse and, 
        if the response is yes, it calls the model function for perform the
         modification in the Databasea and the visual changes in the GUI"""
        self.stop()
        users_response = self.view.delete_patient_view_question()
        if users_response == QMessageBox.Ok:
            patient_id = int(self.view.information_labels_objects["patient_id_label"].text())
            self.model.delete_patient_model((patient_id,))
            data_table_name = self.model.return_experiences_id_by_patient((patient_id,))
            for i in data_table_name:
                self.model.delete_table(int(i[0]))
            self.view.delete_patient_done_view()
            self.view.clear_canvas_view()
            self.disable_all_snap_buttons()
            self.clear_plot_data_second_window()
            self.clear_plots_data_mainwindow_controller()
        elif users_response == QMessageBox.No:
            return

    def show_next_patient_controller(self):
        """ Function that allow to navigate throught the Database, patient 
        by patient. It also set all the values to 0 for restart a 
        recording session for the new patient"""
        self.stop()
        database_patients_id_list = self.model.return_all_patients_id()
        database_patients_id_list =list(flatten(database_patients_id_list))
        current_patient_id = self.view.information_labels_objects["patient_id_label"].text()
        if current_patient_id.isnumeric():
            current_patient_index = database_patients_id_list.index(int(current_patient_id))
            try:
                next_patient_id = database_patients_id_list[int(current_patient_index) + 1]
                next_patient = self.model.return_patient_by_id((next_patient_id,))
            except IndexError:
                next_patient = self.model.return_patient_by_id((database_patients_id_list[0],))
        else:
            next_patient = self.model.return_patient_by_id((database_patients_id_list[0],))
        next_patient =list(flatten(next_patient))
        patients_id = next_patient.pop(0)
        next_patient.insert(2,patients_id)
        self.model.current_patient = next_patient
        self.enable_buttons(self.view.widget.update_button, self.view.widget.delete_patient_button)
        for i in range(0,len(next_patient)):
            self.view.information_labels_objects[self.view.information_labels[i]].setText(str(next_patient[i]))
        self.view.clear_canvas_view()
        self.disable_all_snap_buttons()
        self.clear_plot_data_second_window()
        self.clear_plots_data_mainwindow_controller()
        
    def show_previous_patient_controller(self):
        """ Function that allow to navigate throught the Database, patient by 
        patient. It also set all the values to 0 for restart a 
        recording session for the new patient"""
        self.stop()
        database_patients_id_list = self.model.return_all_patients_id()
        database_patients_id_list =list(flatten(database_patients_id_list))
        current_patient_id = self.view.information_labels_objects["patient_id_label"].text()
        if current_patient_id.isnumeric():
            current_patient_index = database_patients_id_list.index(int(current_patient_id))
            try:
                previous_patient_id = database_patients_id_list[int(current_patient_index) - 1]
                previous_patient = self.model.return_patient_by_id((previous_patient_id,))
            except IndexError:
                previous_patient = self.model.return_patient_by_id((database_patients_id_list[0],))
        else:
            previous_patient = self.model.return_patient_by_id((database_patients_id_list[-1],))
        previous_patient =list(flatten(previous_patient))
        patients_id = previous_patient.pop(0)
        previous_patient.insert(2,patients_id)
        self.model.current_patient = previous_patient
        self.enable_buttons(self.view.widget.update_button, self.view.widget.delete_patient_button)
        for i in range(0,len(previous_patient)):
            self.view.information_labels_objects[self.view.information_labels[i]].setText(str(previous_patient[i]))
        self.view.clear_canvas_view()
        self.disable_all_snap_buttons()
        self.clear_plot_data_second_window()
        self.clear_plots_data_mainwindow_controller()

    ####### Functions related to the menubar buttons
    def menu_buttons_controller(self,q):
        """central function that recive the name of the pressed tab in the menu bar, and call
        the specific function for that tab"""
        if q.text() == "New":
            os.execl(sys.executable, sys.executable, *sys.argv) # line for reestart the programm
        elif q.text() == "Load":
            self.load_data_controller() 
            # calls the function to open a file previusly created with this program
        elif q.text() == "Patients Database":
            self.open_patients_database_window()
        elif q.text() == "Experiences Database":
            self.open_experiences_database_window()
        else:
            sys.exit()

    ####### FUnctions related to the databases display on the GUI and buttons actions
    def open_patients_database_window(self):
        """ Function that creates the patient database object to be displayed in the GUI,
         along with returning from the model a list with all the patients in the database"""
        sender = "patients_database_window"
        if self.table_database_window is not None:
            self.table_database_window.close()
        complete_database, columns_names = self.model.return_all_patients_database_model()
        self.table_database_window = PatientsExperiencesDatabase(self, complete_database,columns_names,sender)
        self.table_database_window.show()

    def open_experiences_database_window(self):
        """ Function that creates the experiences database object to be displayed in the 
        GUI, along with returning from the model a list with all the experiences in
        the database"""

        sender = "experiences_database_window"
        complete_database, columns_names = self.model.return_all_experiences_database_model()
        if self.table_database_window is not None:
            self.table_database_window.close()
        self.table_database_window = PatientsExperiencesDatabase(self, complete_database,columns_names,sender)
        self.table_database_window.show()
    
    def set_current_patient_controller(self):
        """"This function is triggered when the button  self.widget.set_patient from the 
        self.table_database_window object. This allows to set as the current patients 
        the selected entry from the database, setting all the values from the patients 
        information box of the mainwindow with the information from the selected patient. 
        It also set all the values to theirs base values to restart a new recording session"""
        self.stop()
        patient_info_list = self.table_database_window.set_current_patient_view()
        if patient_info_list == None:
            pass
        else:
            for j, (i1,i2) in zip(patient_info_list, self.view.information_labels_objects.items()):
                i2.setText(j)
            self.model.current_patient = patient_info_list
        self.view.clear_canvas_view()
        self.disable_all_snap_buttons()
        self.clear_plots_data_mainwindow_controller()
    
    def delete_selected_patient_controller(self):
        """ Function for the self.widget.delete_patient button ,from the self.table_database_window 
        object. that calls the model function for perform the selected patient delete in the 
        Database and the visual changes in the GUI"""
        patient_id = self.table_database_window.delete_selected_patient_view()
        if patient_id == None:
            pass
        else:
            data_table_name = self.model.return_experiences_id_by_patient((patient_id,))
            for i in data_table_name:
                self.model.delete_table(int(i[0]))
            self.model.delete_patient_model((patient_id,))
            if str(patient_id) == self.view.information_labels_objects["patient_id_label"].text():
                for i , j in self.view.information_labels_objects.items():
                    if i == "date_label":
                        j.setText(QDateTime.currentDateTime().toString())
                    else:
                        j.setText("_____________________________________________________") 
                self.view.clear_canvas_view()
                self.disable_all_snap_buttons()
                self.clear_plot_data_second_window()
                self.clear_plots_data_mainwindow_controller()
            self.open_patients_database_window()

    def open_selected_experience_controller(self):
        """ This functions is triggered by self.widget.load_experience button 
        from the self.table_database_window object. It cals the model for return all
        the values from the experience to be displayed in the thirdwindow """
        experience_id, patients_id = self.table_database_window.open_selected_experience_view()
        if experience_id == None:
            pass
        else:
            plot_data = self.model.return_experience_table_values_model(int(experience_id))
            patients_info = self.model.return_patient_by_id((patients_id,))
            patients_info = list(flatten(patients_info))
            patients_id = patients_info.pop(0)
            patients_info.insert(2,patients_id)
            self.model.load_values_from_database(plot_data, patients_info)
            self.open_third_window()

    def delete_selected_experience_controller(self):
        """ Function for the self.widget.delete_experience,from the self.table_database_window 
        object. it calls the model function for perform the selected experience delete in the
        Database and the visual changes in the GUI"""
        experience_id = self.table_database_window.delete_selected_experience_view()
        if experience_id == None:
            pass
        else:
            self.model.delete_table(int(experience_id))
            self.model.delete_experience_model((experience_id,))
            self.open_experiences_database_window()
            
    ####### FUnctions related to the load of data from a existing export files coming from this programm
    def load_data_controller(self):
        """ function related to the load button from the menu bar, that opnes a fileinput dialog
        that return the name and path of the load file. after that it calls the model to charge 
        the data and open a third window object to display the data"""
        file_name= self.view.load_filedialog_mainwindow() # returns a tuple with [0] as the file path
        if len(file_name[0]) == 0: 
            # recognizes if the user leave the load file window without entering a file
            return
        else:
            file_name = file_name[0]
            self.model.load_data_model(file_name)
            self.open_third_window()
    
    
    def open_third_window(self):
        """ Opens the thirdwindow object , and if there is an already existing object, ir close
         it and opens another with the recently loaded data """
        if self.third_window is not None:
             self.third_window.close()
        self.third_window = ThirdWindow(self)
        self.third_window.show()
    
    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
        return os.path.join(base_path, relative_path)

if __name__ == '__main__':
    #start the GUI
    app = QApplication(sys.argv)
    controller = Controller()
    app.exec_()

