# "Programme d'acquisition de la pression - LNC Laboratoire de neurosciences cognitives UMR 7291 - AMU - MARSEILLE"

## Name

Ear Pression Datalogger (EPD) project

## Description

This program has for was conceived to registrer the variations of pression of the middle ear, and with this, help for the diagnostic
of specific diseases like TENSON TIMPANI SINDOME (TTS) or any other that could affect the structures linked to the pressure of the middle ear (such as the eardrum, eustachian tube, stapedius muscle, among others).
It allow a long term pressure registration od the middle ear, and ir also can display the data graphs in different formats, 
even allowing the user to export the data for further examination.

## Visuals

1. ![FIG 1](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/FIG1.png)
2. ![FIG 2](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/FIG2.png)
3. ![FIG 3](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/FIG3.png)
4. ![FIG 4](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/FIG4.png)
5. ![FIG 5](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/FIG5.png)
6. ![FIG 6](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/FIG6.png)



## Installation

1. --> Install anaconda [Here](https://docs.anaconda.com/anaconda/install/windows/)
2. --> Download from the repository  and Load the enviroment in anaconda [Here](http://git.lnc.dcs.univ-amu.fr/service-electronique-et-mecanique/projet-arnaud-norena/logiciel-aquisition-et-affichage/-/blob/Morales_Gabriel/Datalogger_env.yaml).
3. --> clone the repository in your local machine [Here](http://git.lnc.dcs.univ-amu.fr/service-electronique-et-mecanique/projet-arnaud-norena/logiciel-aquisition-et-affichage.git) [or_Here](https://gitlab.com/gmoralest/dataloger_pression.git)
4. --> Open the CMD Promt in anaconda
5. --> Access to the cloned directory
6. --> Run the code in the prompt:  **pyinstaller --onefile --add-data "save_logo.png;." --add-data "close_logo.png;." --add-data "ok_logo2.png;." --add-data "add_logo.png;." --add-data "edit_logo.png;." --add-data "cancel_logo.png;." --add-data "delete_logo.png;." --add-data "next_arrow.png;." --add-data "previous_arrow.png;." --add-data "mark_logo.png;." --add-data "reset_logo.png;." --add-data "stop_record.png;." --add-data "record_logo.png;." --add-data "snapshot_logo.png;." --add-data "pause_logo.png;." --add-data "start_logo2.png;." controller_dataloger_pression.py**
7. --> Connect the Pression Data logger into the computer
8. --> Run the resulting  .exe file

## Usage

### Initiate with the program

1. --> Step 1 : Connect the pression capture device into one of the USB - ports available in the computer

3. --> Step 2: Execute the Datalogger program

3. --> Step 3 : A menu with all the available devices and ports will be display in the window, you must select the capture device (in case that the user have the arduino app installed in his computer, the name of the device will be “ Arduino Uno “, otherwise, the USB option). In the case that the  capture device is not connected, the program will show a critical error message and automatically exit the program.

4. --> Step 4: A menu with the question “ Would you like to load an existing database?” will be displayed.
- In the case of “OK”:
	5. --> Step 3.1.1: A menu for selecting a database will be displayed, and a .db file must be selected. If the user select another type of file, the file selection menu will keep appearing until the user close the window or press the cancel button, in which case, a default database will be created in the live memory (all the data recorded in that session will no be saved in any database)

- In the case of “No”:
	6. --> Step 3.2.1: A new menu will be displayed for the user to enter the name of the new database. If this window is closed or the user presses cancel, a default database will be created in the live memory (all the data recorded in that session will not be saved in any database).

The main window of the programme will be available for the user to interact with. The scheme of the window is presented in Fig 1.

The software is divided into 4 windows that display different types of information and are actioned by different users choice. A scheme of the mainwindow of the program can be found in Fig 1. It contains different components that allow the user to interact with. First, there is a menu bar (Fig 1, Box 1), which displays the options of new recording sessions, load data, complete databases display (patients and experiences) and quit the program. The patient's information box (Fig 1, Box 2) displays the information of the current patients the user has set. The edit database box (Fig 1, Box 3) allows the user to perform changes into the database and to change the current patient the program is working with. The program operation box (Fig 1, Box 4) join together all the basic options of graphs display (like the start, stop or reset buttons) along with the session recording options (Star and stop recording buttons). Each graph display zone is related to different functionalities of the software. The  AX1 plot graph (Fig 1, Box 8)  displays the totality of the current session, showing the user a time window of 3 seconds in the graph. The program manages to display the information coming from the data logger at a 100ms rate, with no session time limit. The  AX2 plot graph (Fig 1, Box 9) will display a modifiable by the user  time window (default time 10 seconds) with the last data displayed in the AX1 plot graph.  The axis of both graphs can be modified according to the user's preferences using the ranges manager box  (Fig 1, Box 6). The mini snaps plots zone (Fig 1, Box 10) correspond to the part of the canvas that will display the screen shots taken by the user. This screenshot is a reduced version of the data displayed in the AX2 plot graph,  and allows the user to keep track of significant events that occur during the session. The second window is reserved for the display of the selected mini snaps using the snaps manager (Fig 1, Box 7), it contains several exporting options, along with a section for reports generation. The third window is reserved for the display loaded files generated by the program itself. It also contains a patient's information box that displays the information of the loaded patients and a  range manager box to adapt the graphs to the users preferences. The fourth window correponspond a the display of the programs databases in which the user can perform the search of an specific entry, set a patient as a current patient to work with, load an specific experience or  delete an specific entry.


### Features description:

#### Graphics manipulation

##### Start data displaying

To start with the data display, the user must press the start button (Fig 1, Box 4.a). This will make the program start displaying in real-time the pressure variation captured by the device. All the data from the session will be kept in the AX1 plot graph (Fig 1, Box 8), but only a 2 seconds time frame will be displayed on the screen. To access the rest of the graph, the user can use the scroll bar  (Fig 1, Box 5), in order to navigate through it. Simultaneously, the AX2 plot graph(Fig 1, Box 9) will be displayed, and a time window defined by the user will appear, showing the last X seconds of the AX1 plot graph (default time window of 10 seconds). 
Both graphs can be set to the base values using the Right click of the Mouse. In the case of a right click in the AX1 plot graph, both graphs will restart. On the other hand, if the right click is  placed in the AX2 plot graph, only the  values of this graph will reseted .

##### Stop data displaying

To stop the data displaying,  the user must press the stop button (Fig 1, Box 4.b). This will stop the AX1 plot graph (Fig 1, Box 8) and AX2 plot graph(Fig 1, Box 9) and will disable all the other buttons from the program. To restart the data displaying, the user must press again the star button, reactivating the other buttons. 

##### Screen capture

The Snapshot button  (Fig 1, Box 4.c) allows the user to perform a screen capture from the AX2 plot graph, allowing him to save a maximum of 6 mini snaps images in the mini snaps window (Fig 1, Box 10) . When the maximum of mini snaps is reached, the new graphs will start to overwrite the slots of the previously existing mini snaps. The title of each one of the minisnaps can be modified by clicking with the left button over the graph. This will display a window to enter the new graph titles. To close each minisnap, the user must press it with the right click. Each time , one of the mini snaps appears or disappears, the options in the Snaps manager box  (Fig 1, Box 7.a) will modify showing only the active mini snaps.

##### Reset graphs

In case the user decides to restart the data displaying the same patient, the Reset button (Fig 1, Box 4.f) will set all the values to 0, and will eliminate all the mini snaps (along with the ones open in the second window).

##### Pressure and time range modification

The user can modify the axes of AX1 (pressure range) and AX2 (pressure and time range) plot graphs. For this, a group of combo boxes are displayed in the ranges manager box  (Fig 1, Box 6 ). By default the Y axes (pressure in pascals) of both graphs will be automatically fitted according to the plotted data. In case the user wants to modify the Y axis, he can change the values in the pressure range combobox (Fig 1, Box 6.a for pressure of the AX1 plot graph and Fig 1, Box 6.b for the AX2 plot graph ). In the case of the X axis (time in seconds), this can only be modified for the AX2 plot graph through the time combobox  (Fig 1, Box 6.c). In the case that the user wants to reset the automatically fitted axis option, he can press the “ Auto button” in the case of the pressure range, or can come back to the default y axis limit values of 10 seconds pressing the “default button”.
 
#### Screen capture Display

##### Window opening and manipulation

The already opened mini snaps displayed in the mainwindow can be opened in a second window to improve the visualization. For this, the user can use the snaps manager (Fig 1, Box 7)  to select the mini snaps that will be displayed in a second window. For doing this, he must check the checkbox that corresponds to the mini snap he wants to display. The availability of the different boxes is managed automatically (each time a mini snap is created or deleted, the menu changes). Once he selected the checkboxes, he must press the “Open snaps” button. This will display a second window (Fig 2) in which all the selected snaps will be shown.In any moment, in case the user needs to delete one of the graphs from the second window, it can be done using the close button (Fig 2, Box 1). For each selected and displayed graph, a plot manager box containing the close and saving button will be created for individual manipulation. 

##### Images export

This window allows the user to hold a maximum of 6 graphs, with the option exporting each graph in .JPG format individually (saving button Fig 2, Box 1),  selecting the name and the directory of the user's choice.

##### Report generation

The program allows the user to generate a basic standard report (Fig 6) containing the graphs presented in the second window at the moment of the creation (create report button Fig 2, Box 2), along with the patient's information, and allowing the addition of observations by the user..

#### Data export and load

##### Start recording data

Once the user presses the start button, the option for start recording and saving the data is available. For this, the user must press the Start recording button (Fig 1, Box 4.d). Once it is done, all the data captured from this moment will be saved into the database, and will allow the user to export the data in one of 3 different formats .CSV, .MAT, .TXT). In order for the data to be stored in the database, a patient must be entered and displayed in the current patient information box (Fig 1, Box 2), Otherwise, the user will only have the option of exporting the information but will not be saved into the database.

##### Stop recording data and exportation

This button will stop the recording of the data, displaying an option menu for the user to decide the export format, and the name and directory of the file he wants to create. In this case, the information of the current patient information box (Fig 1, Box 2) will be saved along with the graph data. In case that there is no current patient,  the program will set a default patient with the parameter ”None” in every field..

##### Data load from a file

The files generated by the program that contain the data of the experience (graph data and patient information) can be opened and displayed by the program. For this, the user must select from the menu bar (Fig 1, Box 1), the load tab(Fig 1, Box 1.b).  A new window will open displaying the patient's information (Fig 2, Box 1)  and the AX1 plot graph  (Fig 2, Box 3)  with the data from the recording session. The user can use the scroll bar in order to navigate through the graph, and also modify the ranges of time and pressure using the plot manager boxes (Fig 2, Box 1)  (see the section Graphics manipulation → Start data displaying).
 
##### Data load from the database	

The program allows the user to access the experiences recorded into the database, for this the user must select from the menu bar (Fig 1, Box 1), the experience database tab (Fig 1, Box 1.d). A new window will be displayed containing the information of all the recorded experiences ( Experience ID, patients ID, patient name, data and doctors name). A scheme of the experiences database window is presented in Fig 5.  From here, the user must select an entry from the database by clicking on it and press the “load experience” button from the option box (Fig 3, Box 3). The window containing the loaded data is the same as when we load the data from a file (see the section Data export and load→ Data load from a file). 

#### Database manipulation and display

##### General structure of the database

This program generates a database, which is structured into 2 base tables, one containing the patient information (Patients ID, Patients name, doctors name, date and city), and another one containing the experiences information (experiences id, patients id). The value that serves as a primary key for each table is the respective id, which is automatically generated. In the case of the experiences table, the patient's id serves as a foreing key. There is another group of tables that generates automatically, which are the tables containing the data of each experience (the patients information and the data for tle graphs). This name of each one of this group of tables corresponds to the experiences id.

##### Adding a new patient

The user can add a new patient into the database, this can be performed with the button “add patient” at the edit database box (Fig 1, Box 3). This will display a group of empty input boxes for the fields, doctors name, patients name, date and city. The Patients ID will be automatically generated by the database. The user must enter values into all the fields, otherwise, an error message will appear indicating the error. Once all the fields have been filled, the “Done adding” button must be pressed, in order to finish with the task (This button only appears when adding patient buttons has been pressed).  Once the new patient has been added into the database, his information will be displayed into the patients information box (Fig 1, Box 2).  Also, the program will restart all the base values, setting all the graphs to zero, in order to restart with a new data displaying for the new patient.

##### Editing a patient

The user can edit the information from the database base of the current patient in display at the patients information box (Fig 1, Box 2). For this, he must select the “edit patient” button at the edit database box (Fig 1, Box 3).  This will display a group of input boxes for the fields, doctors name, patients name, date and city, containing the information of the current patient information that can be modified. Patient ID is the only field that can be modified by the user. The user must enter values into all the fields, otherwise, an error message will appear indicating the error. Once all the fields have been filled, the “Done editing” button must be pressed, in order to finish with the task (This button only appears when the edit patient button has been pressed). Once the new patient has been edited from the database, his new information will be displayed into the patients information box (Fig 1, Box 2).  

##### Deleting a patient

The user can delete one of the patients from the database. This task can be performed in 2 ways. The first is through the “delete patient” button at the edit database box (Fig 1, Box 3). Once pressed, this  will display a message, indicating the user that he is about to delete a patient from the database and if he wants to continue with the operation. Once this is done the program will eliminate all the data related to the patient from the database, this includes all the tables from each experience associated with the patients ID, all the entries of experiences associated with the patients ID from the experiences table and from the patients table. The other way to delete a patient is through the patients database window. For this, the user must select from the menu bar (Fig 1, Box 1), the patients database tab(Fig 1, Box 1.c).  A new window will open displaying the complete  patient's database (Fig 4, Box 2).  In this window, the user needs to select the patient's entry he wants to delete. He can navigate through the database using the scrolls bars, or search for an specific entry using the search bar (Fig 4, Box 1). Once the entry has been selected, the user must press the “delete patient” button,   and  the same way as the other method, the program will eliminate all the data related to the patient from the database.


##### Visualization of the patients database

The user can have access to the complete patients database. For this, the user must select from the menu bar (Fig 1, Box 1), the patients database tab (Fig 1, Box 1.c). A new window will open displaying the complete  patient's database (Fig 4, Box 2). The table displayed will only allow the visualization, and the selection of a cell will automatically select the entire row. The user can perform a sorting of the database, using any of the entry fields, by pressing the column name from the table. A search bar (Fig 4, Box 1) can be used to search for a specific entry, entering no matter which information. Finally, in the options box  (Fig 4, Box 3)  the user can choose to set a selected patients as a current patients (this will change the labels from the patients information box patients information box (Fig 1, Box 2) from the mainwindow and restart all the values and graphs to zero to start with a new data displaying) or delete the selected entry (see Database manipulation and display → Deleting a patient).  

##### Visualization of the experiences database

The user can have access to the complete experiences database. For this, the user must select from the menu bar (Fig 1, Box 1), the patients database tab (Fig 1, Box 1.d). A new window will open displaying the complete  experiences database (Fig 5, Box 2).  The table displayed will only allow the visualization, and the selection of a cell will automatically select the entire row. The user can perform a sorting of the database, using any of the entry fields, by pressing the column name from the table. A search bar (Fig 5, Box 1) can be used to search for a specific entry, entering no matter which information. Finally, in the options box  (Fig 5, Box 3)  the user can choose to load the selected experience into a new window ( see Data export and load → Data load from the database ) or delete the selected entry using the “delete experience” button. This action will automatically delete from the database, the experience entry by experience ID and the corresponding table that keeps the data of that experience.

## Demo GIfs

1. ![Real time display](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/first%20running.gif)
2. ![Reset canvas with right click](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/reset%20canvas%20with%20click.gif)
3. ![open mini snaps](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/open%20minisnaps.gif)
4. ![close mini snaps](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/close%20minisnap.gif)
5. ![change title mini snap](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/change%20titlte.gif)
6. ![change graph pressure range](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/change%20range.gif)
7. ![auto pressure range](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/auto%20range.gif)
8. ![change graph time window](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/change%20time%20window.gif)
9. ![open mini snap window](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/open%20minisnap%20window.gif)
10. ![report creation](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/report%20creation.gif)
11. ![reset all canvas](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/reset%20button.gif)
12. ![export data](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/save%20data%20csv.gif)
13. ![load data](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/load%20data.gif)
14. ![Scroll AX1 plot graph](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/scroll%20data%20third%20window.gif)
15. ![Open and search in the database](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/open%20search%20database.gif)
16. ![set current patient](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/set%20patient.gif)
17. ![add patient into the database](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/add%20patient.gif)
18. ![delete patient from the database](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/delete%20patient.gif)
19. ![edit patient from the database](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/edit%20patient.gif)
20. ![navigate the database](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/navigate%20database.gif)
21. ![load graphs from the database](https://gitlab.com/gmoralest/dataloger_pression/-/blob/main/load%20from%20database.gif)



## Support

In case of requierinf assistance, please contact Morales Gabriel, at the mails gmoralest@gmail.com or gabriel-alejandro.MORALES-TAPIA@etu.univ-amu.fr

## Roadmap

1. --> create encripted version of the exported file to keep the patients privacy.
2. --> Create a version with users accounts and passwords to help them keep track of their data.
3. --> Re-write the script of the Arduino device to work with bluetooh conection
4. --> Create a Androd app with 2 versions, one for the treating doctor and other more simple for the patients
(domestic use) 

## Authors and acknowledgment

@authors : ACHIM SCHILLING ,PALERESSOMPOULLE DANY , MORÉ SIMON, MORALES GABRIEL

## Project status
For now, the development has stopped, most of the required features have been responded. But still, if there is any new ideas, and people that would like to continue with this project, i'm more than happy to help.
