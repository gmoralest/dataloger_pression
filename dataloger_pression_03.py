# -*- coding: utf-8 -*-

"""

@authors :  SCHILLING ACHIM ,PALERESSOMPOULLE DANY , MORÉ SIMON, MORALES GABRIEL

"""

#### GUI for getting Data from Arduino, Achim Schilling 22.03.2021
#### Version 0.1

## install anaconda (spider included in anaconda)
## install pyserial (library not included in anaconda)

############################## import  different libraries
import os, sys
import csv
import re
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import time
import serial.tools.list_ports
import glob
from serial import Serial
from matplotlib.cbook import flatten
import time
from scipy.io import savemat
from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QSpinBox, QLineEdit, QMessageBox, QFileDialog, QDoubleSpinBox, QGroupBox, QGridLayout
###############################'
"""
TODO
-SAVE AS A JPG THE SNAPSHOT
-SAVE THE DATA AS CSV
-SET THE RANGE BUTTONS (TIME AND PASCAL)
- CLOSE BUTTON FOR MINISNAPS
-OPEN A WIDOWS WHEN A MINISNAP IS CLICKED
-OPEN SEVERAL SNAPS INTO THIS WINDOW (CHECK)
-POSIBLE TO SAVE THE DATA AND THE JPG OF THIS SNAPS
-TAGS FOR EVENTS 
-TITLES OF THE GRAPHS
-AXIS LABELS OF THE GRAPHS
-SCROLL THE DATA (SPECIALLY FIRST WINDOW)
-SLIDING WINDOW FOR THE FIRST WINDOW, ABLE TO SCROLL
- MANAGE RESIZE WINDOW

BEYOND
-OPEN FILES AND COMPLETE VISUALIZATION (ALSO ABLE TO SNAP CERTAINS PARTS, WITH FLEXIBLE TIME AND PASCAL RANGE)


"""



###################################Matplotlib Widget (contains different plotting windows)
class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=12, height=5, dpi=100 ):
        self.fig = Figure(figsize=(width, height), dpi=dpi,tight_layout=False, constrained_layout = True) # opens a figure
        self.fig.set_constrained_layout_pads(w_pad=0, h_pad=0.2, wspace=0.1)
        self.ax1 = self.fig.add_subplot(7,4,(1,8)) # 1. subplot
        self.ax2 = self.fig.add_subplot(7,4,(9,16)) # 2. subplot


        self.ax1.set_ylim(auto=True) # make  ax ranges and labels
        self.ax2.set_ylim(auto=True) # make  ax ranges and labels
        self.ax2.set_xlabel('time (s)') 
        self.ax1.set_ylabel('pressure')
        self.ax2.set_ylabel('pressure')
        self.ax1.set_axisbelow(True)
        self.ax1.yaxis.grid(color='gray', linestyle='dashed')
        self.ax1.xaxis.grid(color='gray', linestyle='dashed')
        self.ax2.set_axisbelow(True)
        self.ax2.yaxis.grid(color='gray', linestyle='dashed')
        self.ax2.xaxis.grid(color='gray', linestyle='dashed')
        self.ax1.format_coord = lambda x, y: ""
        self.ax2.format_coord = lambda x, y: ""
        
        
        super(MplCanvas, self).__init__(self.fig)


####################main class for GUI
class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        
        ####### widget layout
        plt.ion()
        self.setMinimumWidth(500)
        self.setMinimumHeight(400)
        self.setWindowTitle("Programme d'acquisition de la pression (LNC Laboratoire de neurosciences cognitives UMR 7291)")

        
        #######initializer variables
        self.buffer = '' # to buffer input values
        self.pascal_unit = 249.08890833333 

        self.values_save_p1 = [] # to save the values p1
        self.values_save_p2 = [] # to save the values p2
        
        
        self.values_save_p1_data_file = [] # variables which collect p1 and p2 and are saved to a file
        self.values_save_p2_data_file = [] # variables which collect p1 and p2 and are saved to a file
        
        self.pressure_range1 = 0
        self.time_range1 = 0
        self.pressure_range2 = 0
        self.time_range2 = 0
        
        self.init_count = 0 # small variable to start not from the very beginning
        
        
        ################## boolean variables serve as switch
        self.snapbool = False
        self.start_save_bool = False
        self.running = False
        self.snap_canvas_number = {"ax3": [False,7,4,(17,18),None] ,"ax4":[False,7,4,(19,20),None], "ax5":[False,7,4,(21,22),None], "ax6":[False,7,4,(23,24),None], "ax7":[False,7,4,(25,26),None], "ax8":[False,7,4,(27,28),None]}
        
        ######################## initialize the matplotlib widget, the plot window
        self.sc = MplCanvas(self, width=12, height=7, dpi=100)

        # Create toolbar to measure the y x values in real time
        self.toolbar = NavigationToolbar(self.sc, self)

        self.groupbox1 = QGroupBox(self)
        self.vbox1 = QHBoxLayout(self.groupbox1)
        self.groupbox1.setLayout(self.vbox1)        

        self.groupbox2 = QGroupBox(self)
        self.vbox2 = QVBoxLayout(self.groupbox2)
        self.groupbox2.setLayout(self.vbox2)
        
        layout = QGridLayout()
        layout.addWidget(self.toolbar, 1,1,1,3)
        layout.addWidget(self.groupbox1, 1,4)
        layout.addWidget(self.groupbox2,2,1)
        layout.addWidget(self.sc,2,2,1,10)
        # Create a placeholder widget to hold our toolbar and the button and so on
        self.widget = QWidget()
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)
        
        
        ####################################add some buttons to placeholder widget
        self.widget.startbutton = QPushButton('Start', self)
        self.widget.startbutton.move(400,15)
        self.widget.startbutton.setStyleSheet('background-color : rgb(51, 255, 64); font: bold 18px')#'green')
        self.widget.startbutton.clicked.connect(self.start)
        self.widget.startbutton.setEnabled(True)
        self.vbox1.addWidget(self.widget.startbutton)

        self.widget.stopbutton = QPushButton('Stop', self)
        self.widget.stopbutton.move(530,15)
        self.widget.stopbutton.setStyleSheet('background-color : rgb(247, 96, 96); font: bold 18px') #red
        self.widget.stopbutton.clicked.connect(self.stop)
        self.widget.stopbutton.setEnabled(False)
        self.vbox1.addWidget(self.widget.stopbutton)
        
        self.widget.snapbutton = QPushButton('Snapshot', self)
        self.widget.snapbutton.move(660,15)
        self.widget.snapbutton.setStyleSheet('background-color : rgb(253, 238, 0);font: bold 14px')
        self.widget.snapbutton.clicked.connect(self.snap)
        self.widget.snapbutton.setEnabled(False)
        self.vbox1.addWidget(self.widget.snapbutton)
        
        self.widget.startsavebutton = QPushButton('Start Saving', self)
        self.widget.startsavebutton.move(790,15)
        self.widget.startsavebutton.clicked.connect(self.start_saving)
        self.widget.startsavebutton.setStyleSheet('background-color : rgb(51, 255, 64);font: bold 14px')
        self.widget.startsavebutton.setEnabled(False)
        self.vbox1.addWidget(self.widget.startsavebutton)
        
        
        self.widget.stopsavebutton = QPushButton('Stop Saving', self)
        self.widget.stopsavebutton.move(920,15)
        self.widget.stopsavebutton.clicked.connect(self.stop_saving)
        self.widget.stopsavebutton.setEnabled(False)
        self.widget.stopsavebutton.setStyleSheet('background-color : rgb(247, 96, 96);font: bold 14px')
        self.vbox1.addWidget(self.widget.stopsavebutton)

        self.widget.resetbutton = QPushButton('Reset', self)
        self.widget.resetbutton.move(1050,15)
        self.widget.resetbutton.clicked.connect(self.reset_canvas)
        self.widget.resetbutton.setEnabled(False)
        self.widget.resetbutton.setStyleSheet('background-color : rgb(52, 201, 235);font: bold 14px')
        self.vbox1.addWidget(self.widget.resetbutton)


        ####################################add Spinboxes, labels and buttons for canvas ax1 display control to placeholder widget
        self.widget.label_pressure1 = QLabel(self)
        self.widget.label_pressure1.setText("Set Pressure \n    range")
        self.widget.label_pressure1.move(50, 100)
        #self.widget.label_pressure1.setScaledContents(True) // MAYBE FOR RESIZE PORPOUSES

        self.widget.autoButton1 = QPushButton('Auto', self)
        self.widget.autoButton1.setGeometry(55,160,45,20)
        self.widget.autoButton1.setStyleSheet('font: bold 10px')
        self.widget.autoButton1.clicked.connect(self.auto_range_canvas_pressure)
        self.widget.autoButton1.setEnabled(False)

        self.widget.doubleSpinBox_pressure1 = QDoubleSpinBox(self)
        self.widget.doubleSpinBox_pressure1.setGeometry(55, 130, 45, 20)
        self.widget.doubleSpinBox_pressure1.setRange(-100, 100)
        self.widget.doubleSpinBox_pressure1.setValue(0.0)
        self.widget.doubleSpinBox_pressure1.setEnabled(False)
        self.widget.doubleSpinBox_pressure1.valueChanged.connect(self.recover_result_pressure_spinbox)

        self.widget.label_time1 = QLabel(self)
        self.widget.label_time1.setText("Set time \n  range")
        self.widget.label_time1.move(55, 180)

        self.widget.doubleSpinBox_time1 = QDoubleSpinBox(self)
        self.widget.doubleSpinBox_time1.setGeometry(55, 210, 45, 20)
        self.widget.doubleSpinBox_time1.setRange(-100, 100)
        self.widget.doubleSpinBox_time1.setValue(0.0)
        self.widget.doubleSpinBox_time1.setEnabled(False)

         ####################################add Spinboxes, labels and buttons for canvas ax2 display control to placeholder widget
        self.widget.label_pressure2 = QLabel(self)
        self.widget.label_pressure2.setText("Set Pressure \n    range")
        self.widget.label_pressure2.move(50, 300)

        self.widget.autoButton2 = QPushButton('Auto', self)
        self.widget.autoButton2.setGeometry(55,360,45,20)
        self.widget.autoButton2.setStyleSheet('font: bold 10px')
        self.widget.autoButton2.clicked.connect(self.auto_range_canvas_pressure)
        self.widget.autoButton2.setEnabled(False)

        self.widget.doubleSpinBox_pressure2 = QDoubleSpinBox(self)
        self.widget.doubleSpinBox_pressure2.setGeometry(55, 330, 45, 20)
        self.widget.doubleSpinBox_pressure2.setRange(0, 100)
        self.widget.doubleSpinBox_pressure2.setValue(0.0)
        self.widget.doubleSpinBox_pressure2.setEnabled(False)
        self.widget.doubleSpinBox_pressure2.valueChanged.connect(self.recover_result_pressure_spinbox)

        self.widget.label_time2 = QLabel(self)
        self.widget.label_time2.setText("Set time \n  range")
        self.widget.label_time2.move(55, 380)

        self.widget.doubleSpinBox_time2 = QDoubleSpinBox(self)
        self.widget.doubleSpinBox_time2.setGeometry(55, 410, 45, 20)
        self.widget.doubleSpinBox_time2.setRange(-100, 100)
        self.widget.doubleSpinBox_time2.setValue(0.0)
        self.widget.doubleSpinBox_time2.setEnabled(False)

        self.show()  ### show everything
        
        #self.current_ports() ### Saves the current USB port name in use

        #######################the timer function calls every 100 msec the measurement rep function -> read values and update plot
        # Setup a timer to trigger the redraw by calling update_plot.
        self.timer = QtCore.QTimer()
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.measurement_rep)
        self.timer.start()

        
    ##################### functions connected to buttons    
    def start(self):
        self.verify_current_ports()
        while True:
            try: 
                print('Start')
                self.serial = Serial(self.current_ports[0], baudrate=115200)
                self.serial.reset_input_buffer()
                self.running = True
                self.disable_buttons(self.widget.startbutton)
                self.enable_buttons(self.widget.stopbutton,self.widget.snapbutton,self.widget.startsavebutton,self.widget.stopsavebutton,
                self.widget.resetbutton, self.widget.autoButton1, self.widget.doubleSpinBox_pressure1,self.widget.doubleSpinBox_time1,
                self.widget.autoButton2, self.widget.doubleSpinBox_pressure2,self.widget.doubleSpinBox_time2 )
                self.current_ports = []
                break
            except  IndexError:
                msg = QMessageBox()
                msg.setWindowTitle("Arduino device not connected")
                msg.setText("Dataloger pression not connected")
                msg.setIcon(QMessageBox.Critical)
                msg.setStandardButtons(QMessageBox.Retry)
                x = msg.exec_()
                print("No valid device connected")
                break
                

            ####################### Return the ports list and, if the device connected is an arduino card, it send the current port numer in use.
    def verify_current_ports(self):
        ports = serial.tools.list_ports.comports()
        self.current_ports = []
        for port, desc, hwid in sorted(ports):
            if re.search("Arduino Uno", desc):
                self.current_ports.append(port)
            #print("{}: {} [{}]".format(port, desc, hwid))

    def stop(self):
        print('Stop')
        self.running = False
        self.init_count = 0
        self.serial.close()
        self.enable_buttons(self.widget.startbutton)
        self.disable_buttons(self.widget.stopbutton,self.widget.startsavebutton, self.widget.stopsavebutton,self.widget.resetbutton )

    def snap(self):
        self.snapbool =  True
        
    def start_saving(self):
        self.start_save_bool  = True
        self.enable_buttons(self.widget.stopsavebutton)
        self.disable_buttons(self.widget.startsavebutton)
        
    def stop_saving(self):
        self.enable_buttons(self.widget.startsavebutton)
        self.disable_buttons(self.widget.stopsavebutton)
        self.start_save_bool  = False
        self.stop()
        fileName = self.saveFileDialog()
        if fileName is not None:
            self.save(fileName)
        
    
    def save(self, fileName):# save function create a dictionary and save as mat file
        print('save')
        save_dict = {}
        save_dict['P1'] = self.values_save_p1_data_file
        save_dict['P2'] = self.values_save_p2_data_file
        
        t = time.localtime()
        current_time = time.strftime("%H_%M_%S", t)
        
       
        savemat(os.path.join(fileName+'_Pressure_data'+ current_time+'.mat'), save_dict, oned_as='row')

        self.values_save_p1_data_file = []
        self.values_save_p2_data_file = []
    
    def saveFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;Text Files (*.txt)", options=options)
        if fileName:
            return fileName

    def reset_canvas(self):
        self.values_save_p1 = [] # to save the values p1
        self.values_save_p2 = [] # to save the values p2
        self.stop()
        self.disable_buttons(self.widget.snapbutton)
        self.clear_canvas()

    def clear_canvas(self):
        #print(self.sc.ax1)
        self.sc.ax1.cla()
        self.sc.ax2.cla()
        for i in self.snap_canvas_number.keys():
                if self.snap_canvas_number[i][4] is not None:
                    self.snap_canvas_number[i][4].remove()
                    self.snap_canvas_number[i][4] = None
                    self.snap_canvas_number[i][0] = False
        self.sc.draw()
    
    def auto_range_canvas_pressure(self):
        sender = self.sender()
        if sender ==  self.widget.autoButton1:
            self.pressure_range1 = 0
            self.widget.doubleSpinBox_pressure1.setValue(0.0)
        else:
            self.pressure_range2 = 0
            self.widget.doubleSpinBox_pressure2.setValue(0.0)
        

    def recover_result_pressure_spinbox(self):
        sender = self.sender()
        if sender == self.widget.doubleSpinBox_pressure1:
            value = self.widget.doubleSpinBox_pressure1.value()
            self.pressure_range1 = value
        else:
            value = self.widget.doubleSpinBox_pressure2.value()
            self.pressure_range2 = value

    
    ######################### Disable the specified buttons 
    def disable_buttons(self, *args):
        for entry in args:
           entry.setEnabled(False)
    

    ######################### Enable the specified buttons 
    def enable_buttons(self, *args):
        for entry in args:
           entry.setEnabled(True)

    
    ######################### start a measurement 
    def measurement_rep(self):
        if self.running:
            self.complete_measurement_repetition()
            

    ######################one complete measurement repetition

    def complete_measurement_repetition(self):
        ###################first read  input from serial port
        values = self.read_data()
        #print(values)
        self.init_count = self.init_count +1
        if  self.init_count > 17: 
            ###################than convert it to ints
            values_p1, values_p2 = self.get_p1_and_p2(values)
            ################ append values to 
            if len(values_p1)>0:
                #print(values_p1)
                self.values_save_p1.append(values_p1)
            if len(values_p2)>0:    
                #print(values_p2)
                self.values_save_p2.append(values_p2)
            
            self.values_save_p1 = list(flatten(self.values_save_p1))
            self.values_save_p2 = list(flatten(self.values_save_p2))
                
            if self.start_save_bool:
                self.values_save_p1_data_file.append(values_p1)
                self.values_save_p2_data_file.append(values_p2)
                self.values_save_p1_data_file = list(flatten(self.values_save_p1_data_file))
                self.values_save_p2_data_file = list(flatten(self.values_save_p2_data_file))

            ##################refresh the plot
            self.refresh_plot(self.pressure_range1,self.pressure_range2 )

        

    #read the data from serial port
    def read_data(self):
        try:
            ##self.buffer += self.serial.read(self.serial.inWaiting()).decode('utf8')
            self.buffer += self.serial.read(self.serial.in_waiting).decode('utf8')
        except UnicodeDecodeError:
            print('Decode Error: Empty String')

        if '\n' in self.buffer:
            temp = self.buffer.split('\n')
            self.buffer = temp[len(temp)-1]
            values = temp[0:len(temp)-1]
            
        else:
            values = []
        #print(values)
        return values
    
    # get the pressure values from string list
    def get_p1_and_p2(self, values):
        values_p1 = []
        values_p2 = []
        #print(values)
        for i, value in enumerate(values):  # '{TIMEPLOT|DATA|Pression2|T|8205}\r'
                                            # {TIMEPLOT|DATA|Pression1|T|8215}
            
            if value[23] == '1':
                #print('########', 'value:', value[0:len(value)-1] + '***', '#####', len(value), int(value[27:len(value)-2]))
                #values_p1.append(int(value[27:len(value)-2]))
                values_p1.append(((((int(value[27:len(value)-2]))-1638)*(2-(-2))/(14745-1638))+(-2))*self.pascal_unit)
            elif value[23] == '2':
                #values_p2.append(int(value[27:len(value)-2]))
                values_p2.append(((((int(value[27:len(value)-2]))-1638)*(2-(-2))/(14745-1638))+(-2))*self.pascal_unit)

        return values_p1, values_p2
    
    #refresh the plot
    def refresh_plot(self, pressure_range1,pressure_range2):
        self.sc.ax1.cla()
        self.sc.ax2.cla()
        ###### ax1 plot eveything
        self.sc.ax1.yaxis.grid(color='gray', linestyle='dashed')
        self.sc.ax1.xaxis.grid(color='gray', linestyle='dashed')
        self.sc.ax1.plot(np.arange(len(self.values_save_p1))/170,self.values_save_p1, color = 'b', label = 'sensor 1')
        self.sc.ax1.plot(np.arange(len(self.values_save_p2))/170,self.values_save_p2, color = 'r',  label = 'sensor 2')
        self.sc.ax1.legend(loc = 1)
        if pressure_range1 != 0:
            self.sc.ax1.set_ylim([pressure_range1,-pressure_range1])
        else:
            self.sc.ax1.set_ylim(auto=True)
        ######ax2 plot (last 5 s)
        time_window = 10
        ## sampling rate=170
        if len(self.values_save_p1) > 170*time_window  + 10:
            self.sc.ax2.set_axisbelow(True)
            self.sc.ax2.plot(np.arange(int((170*time_window )))/170,self.values_save_p1[-int((170*time_window )):], color = 'b')
            self.sc.ax2.plot(np.arange(int((170*time_window )))/170,self.values_save_p2[-int((170*time_window )):], color = 'r')
        self.sc.ax2.yaxis.grid(color='gray', linestyle='dashed')
        self.sc.ax2.xaxis.grid(color='gray', linestyle='dashed')
        self.sc.ax2.set_xlim([0,time_window ])
        if pressure_range2 != 0:
            self.sc.ax2.set_ylim([pressure_range2,-pressure_range2])
        else:
            self.sc.ax2.set_ylim(auto=True) 
        if self.snapbool and (len(self.values_save_p1) > (170*time_window  + 10)): # on attends 10 secondes
            self.snap_dico_checking(self.snap_canvas_number)
            for canvas_id, status in self.snap_canvas_number.items():
                    if status[0] is False:
                        #print(canvas_id + str(status))
                        if status[4] is not None:
                            self.snap_canvas_number[canvas_id][4].remove()
                        self.snap_canvas_number[canvas_id][4] = self.sc.fig.add_subplot(status[1],status[2],status[3])
                        self.snap_canvas_number[canvas_id][4].set_xlim([0,10]) 
                        self.snap_canvas_number[canvas_id][4].set_axisbelow(True)
                        self.snap_canvas_number[canvas_id][4].format_coord = lambda x, y: ""
                        self.snap_canvas_number[canvas_id][4].yaxis.grid(color='gray', linestyle='dashed')
                        self.snap_canvas_number[canvas_id][4].xaxis.grid(color='gray', linestyle='dashed')
                        self.snap_canvas_number[canvas_id][4].plot(np.arange(int((170*time_window )))/170,self.values_save_p1[-int((170*time_window)):], color = 'b')
                        self.snap_canvas_number[canvas_id][4].plot(np.arange(int((170*time_window )))/170,self.values_save_p2[-int((170*time_window)):], color = 'r')
                        self.snap_canvas_number[canvas_id][0] = True
                        self.snapbool =  False
                        break
        self.sc.draw()
     
    def snap_dico_checking(self,dico):
        values = list(dico.values())
        check_list = []
        for i in values:
            check_list.append(i[0])
        if all(i == True for i in check_list):
            for i in self.snap_canvas_number.keys():
                self.snap_canvas_number[i][0] = False
                
            



if __name__ == '__main__':
        
    #start the GUI
    app = QApplication(sys.argv)
    w = MainWindow()
    app.exec_()
    


    
    
    
    