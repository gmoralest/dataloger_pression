# -*- coding: utf-8 -*-

"""

@authors : ACHIM SCHILLING ,PALERESSOMPOULLE DANY , MORÉ SIMON, MORALES GABRIEL

"""

############################## import  different libraries
import os
import csv
import itertools
from matplotlib.cbook import flatten
import numpy as np
from scipy.io import savemat
import scipy.io
import sqlite3
from sqlite3 import Error
import time
###############################

class Model:
    def __init__(self, controller):
            self.controller = controller
            self.buffer = '' # to buffer input values
            self.pascal_unit = 249.08890833333 
            self.values_save_p1 = [] # to save the values p1
            self.values_save_p2 = [] # to save the values p2
            self.values_save_p1_ax2 = [0] * 1700 # to save the values p1
            self.values_save_p2_ax2 = [0] * 1700 # to save the values p2
            self.values_load_p1 = [] # to load the values p1
            self.values_load_p2 = [] # to load the values p2
            self.patient_load_info = []
            self.values_load_markers = np.array([0])
            self.values_save_p1_data_file = [] # variables which collect p1 and p2 and are saved to a file
            self.values_save_p2_data_file = [] # variables which collect p1 and p2 and are saved to a file
            self.init_count = 0 # small variable to start not from the very beginning
            self.current_patient = [None,None,None,None,None]
            self.conn = None # variabe for the creation/connection with the SQLITE Database 
            self.cur = None # variable for manipulate the created/connected SQLITE Database 

    def complete_measurement_repetition(self):
        """One complete measurement repetition"""
        values = self.read_data() # first read  input from serial port
        self.init_count = self.init_count +1
        if  self.init_count > 10: 
            values_p1, values_p2 = self.get_p1_and_p2(values) # Then convert it to ints
            if len(values_p1)>0:
                self.values_save_p1.append(values_p1)
                self.values_save_p1_ax2.append(values_p1)
            if len(values_p2)>0:
                self.values_save_p2.append(values_p2)
                self.values_save_p2_ax2.append(values_p2)
            self.values_save_p1 = list(flatten(self.values_save_p1))
            self.values_save_p2 = list(flatten(self.values_save_p2))
            self.values_save_p1_ax2 = list(flatten(self.values_save_p1_ax2))
            self.values_save_p2_ax2 = list(flatten(self.values_save_p2_ax2))
            self.values_save_p1_ax2 = self.values_save_p1_ax2[-int((170  * self.controller.time_window)):]
            self.values_save_p2_ax2 = self.values_save_p2_ax2[-int((170  * self.controller.time_window)):]
            if self.controller.start_save_bool: # 
                self.values_save_p1_data_file.append(values_p1)
                self.values_save_p2_data_file.append(values_p2)
                self.values_save_p1_data_file = list(flatten( self.values_save_p1_data_file))
                self.values_save_p2_data_file = list(flatten( self.values_save_p2_data_file))

    def read_data(self):
        """ read the data from serial port"""
        try:
            self.buffer += self.controller.serial.read(self.controller.serial.in_waiting).decode('utf8')
        except UnicodeDecodeError:
            pass
            #print('Decode Error: Empty String')
        if '\n' in self.buffer:
            temp = self.buffer.split('\n')
            self.buffer = temp[len(temp)-1]
            values = temp[0:len(temp)-1]
        else:
            values = []
        return values

    def get_p1_and_p2(self, values):
        """get the pressure values from string list"""
        values_p1 = []
        values_p2 = []
        for i, value in enumerate(values):  #  {TIMEPLOT|DATA|Pression2|T|8205}\r
                                            #  {TIMEPLOT|DATA|Pression1|T|8215}
            try:
                if value[23] == '1':
                    values_p1.append(((((int(value[27:len(value)-2]))-1638)*(2-(-2))/(14745-1638))+(-2))*self.pascal_unit)
                elif value[23] == '2':
                    values_p2.append(((((int(value[27:len(value)-2]))-1638)*(2-(-2))/(14745-1638))+(-2))*self.pascal_unit)
                else:
                    pass
            except IndexError : # Error that appears when the microcontroller sents the string with 
                                # format {TIMEPression2|T|8195}  {TIMEPresion2|T|8195}on2|T|8195}
                pass
            except ValueError :
                pass
        return values_p1, values_p2

    def create_connection(self, db_file):
        """create a database connection to a SQLite database, creating the patients table, 
        and the experiences table"""
        self.conn = None
        try:
            self.conn = sqlite3.connect(db_file) #Connecting to sqlite
        except Error as error:
            print(error)
        self.conn.execute("PRAGMA foreign_keys = ON")
        self.cursor = self.conn.cursor()
        patients_table = ''' CREATE TABLE  IF NOT EXISTS patients (Patients_ID INTEGER PRIMARY KEY AUTOINCREMENT,
                Doctors_Name VARCHAR NOT NULL, Patients_Name VARCHAR NOT NULL, Date VARCHAR NOT NULL , City VARCHAR NOT NULL)'''
        experience_table =  ''' CREATE TABLE  IF NOT EXISTS experience (  Experience_ID INTEGER PRIMARY KEY AUTOINCREMENT ,
                 Patients_ID INTEGER, FOREIGN KEY (Patients_ID) REFERENCES patients(Patients_ID))'''  
        self.cursor.execute(patients_table)
        self.cursor.execute(experience_table)
    
    def add_new_patient_model(self, patient_info):
        """ Create and send the INSERT REQUEST THE DATABASE whit the valies entered by the user"""
        sqlite_insert_with_param = ''' INSERT INTO  patients(Doctors_Name, Patients_Name, Date, City) VALUES (?, ?, ?, ?); '''
        self.cursor.execute(sqlite_insert_with_param, patient_info)
        self.conn.commit()

    def update_patient_model(self, patient_new_info):
        """ Create and send the UPDATE REQUEST THE DATABASE whit the valies entered by the user"""
        sqlite_update_with_param = """ UPDATE patients SET Doctors_Name =  ?, Patients_Name =  ?, Date =  ?, City =  ? WHERE Patients_ID =  ? ; """
        self.cursor.execute(sqlite_update_with_param, patient_new_info)
        self.conn.commit()

    def delete_patient_model(self,patient_id):
        """ Create and send the DELETE REQUEST to THE DATABASE whit the valies entered by the user"""
        sqlite_delete_with_param = """DELETE FROM experience WHERE Patients_ID = ? """
        self.cursor.execute(sqlite_delete_with_param, patient_id)
        self.conn.commit()

        sqlite_delete_with_param = """DELETE FROM patients WHERE Patients_ID = ? """
        self.cursor.execute(sqlite_delete_with_param, patient_id)
        self.conn.commit()

    def delete_experience_model(self, experience_id):
        """ Create and send the DELETE REQUEST to THE DATABASE whit the valies entered by the user"""
        sqlite_delete_with_param = """DELETE FROM experience WHERE Experience_ID = ? """
        self.cursor.execute(sqlite_delete_with_param, experience_id)

    def add_new_experience_database_model(self, patient_id):
        """ Create and send the INSERT REQUEST THE DATABASE whit the valies entered by the user"""
        sqlite_insert_with_param = ''' INSERT INTO  experience(Patients_ID) VALUES (?); '''
        self.cursor.execute(sqlite_insert_with_param, patient_id)
        self.conn.commit()

    def return_all_patients_database_model(self):
        """ Create and send the request to select all the patients from the patients 
        table and returns the patients list and the name of the columns"""
        sqlite_select_with_param = ''' SELECT * FROM  patients '''
        self.cursor.execute(sqlite_select_with_param)
        rows =  self.cursor.fetchall()
        headers = []
        for i in self.cursor.description:
            headers.append(i[0])
        return rows , headers
    
    def return_all_patients_id(self):
        """ Create and send the request to select all the patients id  from the patients table"""
        self.cursor.execute(''' SELECT Patients_ID FROM  patients ''')
        patients_ids = self.cursor.fetchall()
        return patients_ids

    def return_patient_by_id(self,patient_id):
        """ Create and send the request to select all information  af a patient from 
        the patients table according to patients ID"""
        sqlite_select_with_param = ''' SELECT * FROM  patients WHERE  Patients_ID = ? '''
        self.cursor.execute(sqlite_select_with_param, patient_id)
        patient = self.cursor.fetchall()
        return patient

    def return_all_experiences_database_model(self):
        """ Create and send the request to select all the experiences id and the values
        asociated to the patients ID foreign key from the experiences and  patients tables"""

        sqlite_select_with_param = ''' SELECT Experience_ID, experience.Patients_ID, Patients_Name,
        Date, Doctors_Name FROM experience INNER JOIN patients ON experience.Patients_ID = patients.Patients_ID; ''' 
        self.cursor.execute(sqlite_select_with_param)
        rows =  self.cursor.fetchall()
        headers = []
        for i in self.cursor.description:
            headers.append(i[0])
        return rows , headers
    
    def return_experiences_id_by_patient(self, patient_id):
        """ Create and send the request to select all the experiences id asociated with an specific patient_id"""
        sqlite_select_with_param = ''' SELECT Experience_ID FROM experience where Patients_ID = ? '''
        self.cursor.execute(sqlite_select_with_param, patient_id)
        experiences = self.cursor.fetchall()
        return experiences
    
    def return_experience_table_values_model(self,data_table_name):
        """ Create and send the request to select all the values from a table with the name of the given experiences id"""
        self.cursor.execute('SELECT * FROM `{tab}`'.format(tab=data_table_name))
        plot_data = self.cursor.fetchall()
        return plot_data

    def create_table_experience_data(self):
        """ Create and send the request to create a talbe with the experience id name for keeping
        the experience data and patients information"""
        data_table_name = str(self.cursor.lastrowid)
        print(data_table_name)
        self.cursor.execute('CREATE TABLE IF NOT EXISTS `{tab}` (P1  FLOAT , P2  FLOAT)'.format(tab=data_table_name))
        self.conn.commit()
        save_dict = {}
        save_dict['P1'] = self.values_save_p1_data_file
        save_dict['P2'] = self.values_save_p2_data_file
        save_query_list = []
        for combination in itertools.zip_longest(save_dict['P1'], save_dict['P2']):
            data = tuple(list(combination))
            save_query_list.append(data)
        qlite_insert_with_param = ('INSERT INTO `{tab}` (P1,P2) VALUES (?,?)'.format(tab=data_table_name))    
        self.cursor.executemany(qlite_insert_with_param, save_query_list)
        self.conn.commit()
    
    def delete_table(self, data_table_name):
        """send the request to the data base to delete a table (for porpouses of this program
         it deletes the tables with the experiences data)"""
        self.cursor.execute('DROP TABLE IF EXISTS `{tab}`'.format(tab=data_table_name))
        self.conn.commit()
    
    def load_values_from_database(self, plot_data, patients_info):
        """ FUnction that set the atributtes to the recovered values"""
        self.values_load_p1 = [] # to load the values p1
        self.values_load_p2 = [] # to load the values p2
        self.patient_load_info = []
        for i in plot_data:
            self.values_load_p1.append(i[0]) # to load the values p1
            self.values_load_p2.append(i[1]) # to load the values p2
        self.patient_load_info = patients_info

    def export_data_model(self, fileName , selected_file_type):
        """ save function create a dictionary and save the data in the selected format """
        save_dict = {}
        save_dict["doctor_name_label"] = self.current_patient[0]
        save_dict["patient_name_label"] = self.current_patient[1]
        save_dict["patient_id_label"] = self.current_patient[2]
        save_dict["date_label"] = self.current_patient[3]
        save_dict["city_label"] = self.current_patient[4]
        save_dict['P1'] = self.values_save_p1_data_file
        save_dict['P2'] = self.values_save_p2_data_file
        t = time.localtime()
        current_time = time.strftime("%H_%M_%S", t)
        if selected_file_type == ".mat": 
            savemat(os.path.join(fileName+'_Pressure_data'+ current_time+'.mat'), save_dict, oned_as='row')
        elif selected_file_type == ".csv":
            with open(os.path.join(fileName+'_Pressure_data'+ current_time+".csv"), 'w', encoding='UTF8') as csv_file:
                # create the csv writer
                writer = csv.writer(csv_file, delimiter=',', lineterminator='\r')
                # save the patients information in the csv file
                for i,j in save_dict.items():
                    if i == 'P1' or i == 'P2':
                        pass
                    else:
                            writer.writerow([j])
                # save the plot information in the csv file
                for combination in itertools.zip_longest(save_dict['P1'], save_dict['P2']):
                    data = list(combination)
                    writer.writerow(data)
        elif selected_file_type == ".txt":
            txt_file= open(fileName + ".txt" ,"w+")
            for i,j in save_dict.items():
                if i == 'P1' or i == 'P2':
                    pass
                else:
                   txt_file.write(str(j)+ "\n")           
            for combination in itertools.zip_longest(save_dict['P1'], save_dict['P2']):
                data = (str(combination) + "\n")
                txt_file.write(data)
            txt_file.close()
        self.values_save_p1_data_file = []
        self.values_save_p2_data_file = []

    def export_data_model_second_window(self, fileName, plot_data,all_data_dico):
        t = time.localtime()
        current_time = time.strftime("%H_%M_%S", t)
        save_dict = {}
        save_dict["doctor_name_label"] = self.current_patient[0]
        save_dict["patient_name_label"] = self.current_patient[1]
        save_dict["patient_id_label"] = self.current_patient[2]
        save_dict["date_label"] = self.current_patient[3]
        save_dict["city_label"] = self.current_patient[4]
        
        if all_data_dico == None:
            save_dict['P1'] = plot_data[0]
            save_dict['P2'] = plot_data[1]
            
            with open(os.path.join(fileName+'_Pressure_data'+ current_time +".csv"), 'w', encoding='UTF8') as csv_file:
                    # create the csv writer
                    writer = csv.writer(csv_file , delimiter=',', lineterminator='\r')
                    # save the patients information in the csv file
                    for i,j in save_dict.items():
                        if i == 'P1' or i == 'P2':
                            pass
                        else:
                                writer.writerow([j])
                    # save the plot information in the csv file
                    for combination in itertools.zip_longest(save_dict['P1'], save_dict['P2']):
                        data = list(combination)
                        writer.writerow(data)
        
        elif isinstance(all_data_dico,dict):
            with open(os.path.join(fileName+'_Pressure_data'+ current_time +".csv"), 'w', encoding='UTF8') as csv_file:
                    # create the csv writer
                    writer = csv.writer(csv_file, delimiter=',', lineterminator='\r')
                    # save the patients information in the csv file
                    for i,j in save_dict.items():
                        if i == 'P1' or i == 'P2':
                            pass
                        else:
                                writer.writerow([j])

                    data = list(all_data_dico.keys())
                    writer.writerow(data)
                    
                    # save the plot information in the csv file
                    for combination in itertools.zip_longest(*all_data_dico.values(), fillvalue=0):
                        data = list(combination)
                        writer.writerow(data)

    def load_data_model(self,file_name):
        """ it takes the selected file, and load the values to be displayed in the thirdwindow"""
        self.values_load_p1 = [] # to load the values p1
        self.values_load_p2 = [] # to load the values p2
        self.patient_load_info = []
        name, extension = os.path.splitext(file_name)
        if extension == ".csv": 
            file = open(file_name)
            patients_info = []
            csvreader = csv.reader(file)
            counter = 0
            for row in csvreader:
                if counter in [0,1,2,3,4]:
                    patients_info.append(row)
                    counter += 1
                elif len(row) == 2 and row[0] != "" and row[1] != "":
                    self.values_load_p1.append(float(row[0]))
                    self.values_load_p2.append(float(row[1]))
                    counter += 1
                else:
                    counter += 1
            self.patient_load_info = flatten(patients_info)
        elif extension == ".mat":
            patients_info = []
            file = scipy.io.loadmat(file_name)
            for i in list(flatten(file['P1'])):
                self.values_load_p1.append(i)
            for j in list(flatten(file['P2'])):
                self.values_load_p2.append(j)
            patients_info_labels = ["doctor_name_label","patient_name_label","patient_id_label",
                                    "date_label","city_label"]
            for i in patients_info_labels:
                patients_info.append(file[i])
            self.patient_load_info = flatten(patients_info)
        elif extension == ".txt":
            patients_info = []
            char_to_replace = {'(': '',')': ''}
            file = open(file_name, "r")
            counter = 0
            for line in file:
                line = line.rstrip("\n")
                for key, value in char_to_replace.items():
                    line = line.replace(key, value)
                line = line.split(",")
                if counter in [0,1,2,3,4]:
                    patients_info.append(line)
                    counter += 1
                else:
                    if line[0] == " None":
                        pass
                    else:
                        self.values_load_p1.append(float(line[0]))
                        counter += 1
                    if line[1] == " None":
                        pass
                    else:
                        self.values_load_p2.append(float(line[1]))
                        counter += 1
            self.patient_load_info = flatten(patients_info)


            