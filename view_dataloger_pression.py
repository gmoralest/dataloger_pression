# -*- coding: utf-8 -*-

"""
@authors : ACHIM SCHILLING ,PALERESSOMPOULLE DANY , MORÉ SIMON, MORALES GABRIEL
"""
############################## import  different libraries

from functools import partial
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import numpy as np
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import QDateTime, Qt,QSize
from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QPushButton
from PyQt5.QtWidgets import QFileDialog,QDoubleSpinBox, QGroupBox, QGridLayout, QScrollBar
from PyQt5.QtWidgets import QComboBox, QMenuBar, QLabel, QInputDialog, QDateTimeEdit
from PyQt5.QtWidgets import QAction,QMessageBox, QLineEdit, QTableWidget,QHeaderView
from PyQt5.QtWidgets import QTableWidgetItem, QTableView, QCheckBox
from model_dataloger_pression import *

###### Matplotlib Widgets (contains different plotting windows)
####### CANVAS FOR MAINWINDOW

class MplCanvas(FigureCanvasQTAgg):
    """ This class represent the canvas of the mainwindow. It creates the Figure 
    object where the plots will be placed. From the beggining, it creates
    plots AX1, where all the real time data will be displayed, and the AX2,
    where an specified time window of the AX1 plot will be displayed"""

    def __init__(self, parent=None, width=12, height=5, dpi=100 ):
        self.fig = Figure(figsize=(width, height), dpi=dpi,tight_layout=True)
        self.axes_dico_mplcanvas = {"ax1": [7,4,(1,8)],"ax2": [7,4,(9,16)]}
        self.ax1 = None
        self.ax2 = None
        for j, i in self.axes_dico_mplcanvas.items():
            setattr(self, j,  self.fig.add_subplot(i[0],i[1],i[2])) # AX1. subplot, 
            #the parammeters A,B,(C,D) of the figure.add_subplot correspond to the 
            # size of the plot (A x B) and the positions occupied by the subplot (from C to D) 
            temp_ax = getattr(self, j)
            temp_ax.set_ylim(auto=True) # make  ax ranges and labels
            temp_ax.set_ylabel('Pressure')
            temp_ax.set_axisbelow(True)
            temp_ax.yaxis.grid(color='gray', linestyle='dashed')
            temp_ax.xaxis.grid(color='gray', linestyle='dashed')
            if j == "ax2":
                temp_ax.set_xlabel('Time (s)')
        super(MplCanvas, self).__init__(self.fig)

####### CANVAS FOR SECONDWINDOW
class MplCanvas_secondwindow(FigureCanvasQTAgg):
    """ This class represent the canvas of the second window  object. It creates
    the Figure object where the plots will be placed. The number of plot 
    dependends on how many snapshot have been clicked by the user. It display 
    the acive snapshot. """
    def __init__(self, secondwindow ,controller , width=12, height=5, dpi=100 ):
        self.controller = controller
        self.fig = Figure(figsize=(width, height), dpi=dpi,tight_layout=True) # opens a figure
        count = 1
        self.axes_list = []
        snap_count = 0
        for i in controller.snap_canvas_number_second_window.values():
            if i[1] == True:
                snap_count += 1
        for j ,i in controller.snap_canvas_number_second_window.items(): 
            # it checks the list of active plots for the second window, and creates a list with
            # the objects tobe opne in the second window
            if i[1] == True: 
                if  snap_count >= 4:
                    setattr(self.fig,i[0],self.fig.add_subplot(snap_count,1,count)) 
                    # create one subplot for each active snapshot
                else:
                    setattr(self.fig,i[0],self.fig.add_subplot(3,1,count)) 
                    # create one subplot for each active snapshot
                try:
                    self.axes_list.append([i[0],i[2][2],i[2][0], i[2][1],i[2][3]])
                    count += 1
                except TypeError:
                    return 
        for k in self.axes_list: # in each plot, it displays the data from the snapshots    
            time_window = k[1]
            temp_axis = getattr(self.fig,k[0])
            temp_axis.plot(np.arange(int((170*time_window )))/170,k[2], color = 'b')
            temp_axis.plot(np.arange(int((170*time_window )))/170,k[2], color = 'b')
            temp_axis.plot(np.arange(int((170*time_window )))/170, k[3], color = 'r')
            temp_axis.yaxis.grid(color='gray', linestyle='dashed')
            temp_axis.xaxis.grid(color='gray', linestyle='dashed')
            temp_axis.title.set_text(k[4])
            temp_axis.set_ylabel('Pressure', fontsize = 8 )
            temp_axis.set_xlabel('Time (s)')
        super(MplCanvas_secondwindow, self).__init__(self.fig)

####### CANVAS FOR THIRDWINDOW

class MplCanvas_Thirdwindow(FigureCanvasQTAgg):
    """ This class represent the canvas of the second third object. it 
    creates a figure with just one subplt to display the data loaded from 
    the loaded file. """

    def __init__(self, thirdwindow ,controller, width=12, height=5, dpi=100 ):   
        self.controller = controller
        self.fig = Figure(figsize=(width, height), dpi=dpi,tight_layout=True) # opens a figure
        
        self.ax1 = self.fig.add_subplot(7,4,(1,16)) # 1. subplot
        self.ax1.set_ylim(auto=True) # make  ax ranges and labels
        self.ax1.set_ylabel('Pressure')
        self.ax1.set_axisbelow(True)
        self.ax1.yaxis.grid(color='gray', linestyle='dashed')
        self.ax1.xaxis.grid(color='gray', linestyle='dashed')
        super(MplCanvas_Thirdwindow, self).__init__(self.fig)

####### Creates a Toolbar from NvigationToolbar where the coordinates messages are eliminate
class MyToolbar(NavigationToolbar):
    def set_message(self, msg):
        """ This function eliminates the coordinates display from the original
         NavigationToolbar object """
        pass

####### MAIN class for GUI
class MainWindow(QMainWindow):
    """ Class for create the mainwindow object for displaying the QTPY widgets 
    and canvas for the user to interact with"""
    def __init__(self, controller,  *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.controller = controller
        self.information_labels_case = [["doctor_name_label_case","Doctors Name :"],
                                        ["patient_name_label_case","Patients Name :"],
                                        ["patient_id_label_case","Patients ID :"],
                                        ["date_label_case","Date :"],
                                        ["city_label_case","City :"]]
        self.information_labels = ["doctor_name_label","patient_name_label",
                                "patient_id_label","date_label","city_label"]
        self.edit_fields = ["doctor_name_line_edit", "patient_name_line_edit", 
                            "patient_id_line_edit",  "date_line_edit", "city_label_line_edit"]
        self.edit_fields_objects = {}
        self.information_labels_objects = {}  
        self.information_labels_case_objects = {}
        ####### widget setting 
        plt.ion()
        self.setMinimumWidth(500)
        self.setMinimumHeight(400)
        self.setWindowTitle("Programme d'acquisition de la pression (LNC Laboratoire de neurosciences cognitives UMR 7291)")
        self.showMaximized()
        ####### initialize the matplotlib widget, the plot window
        self.screen_canvas = MplCanvas(self, width=12, height=7, dpi=100)
        self.screen_canvas.mpl_connect('button_press_event', controller.manage_canvas_click)
         # connect the function every time the canvas is clicked
        
        ####### Create Scrollbar to move through the fist plot
        self.scroll_bar = QScrollBar(QtCore.Qt.Horizontal)
        self.scroll_bar.setValue(99)
        self.scroll_bar.setEnabled(False)
        self.scroll_bar_step = .1
        self.scroll_bar.setPageStep(self.scroll_bar_step * 100)
        self.scroll_bar.sliderReleased.connect(self.update_slider_mainwindow_view)
        self.setup_slider()
        
        #######  Create a menu bar
        self.menubar = QMenuBar()
        actionFile1 = self.menubar.addMenu("File")
        actionFile1.addAction("New")
        actionFile1.addSeparator()
        load = QAction("Load",self) 
        actionFile1.addAction(load)
        actionFile1.triggered[QAction].connect(self.controller.menu_buttons_controller)
        actionFile1.addSeparator()
        actionFile1.addAction("Patients Database")
        actionFile1.addSeparator()
        actionFile1.addAction("Experiences Database")
        actionFile1.addSeparator()
        actionFile1.addAction("Quit")
        actionFile2 = self.menubar.addMenu("Help")
        actionFile2.addAction("README")
        actionFile2.triggered[QAction].connect(self.controller.menu_buttons_controller)

        ####### Creates the Groupboxes for reagrupe widgets with different functions
        self.groupbox_dico_mplcanvas = {"groupbox_buttons": ["vbox_buttons","QGroupBox { background-color: rgb(224, 224, 224) }", None],
                                        "groupbox_patients_info_case" : ["vbox_patients_info_case", "QGroupBox { background-color: rgb(224, 224, 224) }", 'Fields', None],
                                        "groupbox_patients_info" : ["vbox_patients_info",None,'Personal data'],
                                        "groupbox_patients_info_editing" : ["vbox_patients_info_editing" ,"QGroupBox { background-color: rgb(224, 224, 224) }",'Edit', None],
                                        "groupbox_pressure_range_canvas1" : ["vbox_pressure_range_canvas1",None,'Set Pressure range'],
                                        "groupbox_pressure_range_canvas2" : ["vbox_pressure_range_canvas2" ,None , 'Set Pressure range'],
                                        "groupbox_time_range_canvas2" : ["vbox_time_range_canvas2" ,None ,'Set time range'],
                                        "groupbox_event_manager" : ["vbox_event_manager" ,None ,'Event Manager'],
                                        "groupbox_snaps_manager" : ["vbox_snaps_manager" ,None ,'Snaps Manager']
                                        }
                                        #FORMAT --> GROUPBOX NAME : VBOX NAME, APPEAREANCE SETTINGS, BOX TITLE 

        for i, j in  self.groupbox_dico_mplcanvas.items():
            setattr(self, i, QGroupBox(self))
            if i == "groupbox_buttons":
                setattr(self, j[0],QHBoxLayout(getattr(self, i)))
            else:
                setattr(self, j[0],QVBoxLayout(getattr(self, i)))
            if i in ["groupbox_buttons", "groupbox_patients_info_case","groupbox_patients_info_editing" ]: 
                getattr(self, i).setStyleSheet(j[1])
            if i in ["groupbox_patients_info_case","groupbox_patients_info","groupbox_patients_info_editing", 
            "groupbox_pressure_range_canvas1", "groupbox_pressure_range_canvas2", "groupbox_event_manager","groupbox_snaps_manager", "groupbox_time_range_canvas2"]:
                getattr(self, i).setTitle(j[2])
            if i in ["groupbox_patients_info_case","groupbox_patients_info" ]:
                getattr(self, j[0]).setSpacing(0)
            getattr(self, i).setLayout(getattr(self, j[0]))
        
        ####### Layout creation for placing the different widgets in the mainwindow
        layout_dico_mainwindow = {"menubar": [0, 0,0,12], 
                                "groupbox_patients_info_case" : [0,2,1,2],
                                "groupbox_patients_info": [0,4,1,6],
                                "groupbox_patients_info_editing": [0,10,1,1],
                                "scroll_bar": [1,1,1,11],
                                "groupbox_buttons":[0,11,1,1],
                                "groupbox_pressure_range_canvas1":[2,0,1,1],
                                "groupbox_pressure_range_canvas2": [4,0,1,1],
                                "groupbox_time_range_canvas2": [5,0,1,1],
                                #"groupbox_event_manager": [6,0,1,1],
                                "screen_canvas" : [2,1,7,11],
                                "groupbox_snaps_manager": [6,0,1,1]
                                }
                                # FORMAT --> OBJECT NAME :  ROW, COLUMN, NUMBER OF ROW USED, NUMBER OF COLUMN USED 
        layout = QGridLayout()
        for i , j in layout_dico_mainwindow.items():
             layout.addWidget(getattr(self,i), j[0],j[1],j[2],j[3])

        ####### Create a placeholder widget to hold our buttons and so on
        self.widget = QWidget()
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)
        
        ####### Add some Labels for identify patient info and doctor
        label_font = QtGui.QFont('Arial', 10)
        label_font.setBold(True)
        for i in self.information_labels_case:
            setattr(self.widget, i[0],  QLabel(i[1],self))
            self.vbox_patients_info_case.addWidget(getattr(self.widget, i[0]))
            getattr(self.widget, i[0]).setFont(label_font)
            self.information_labels_case_objects[i[0]] = getattr(self.widget, i[0])

        ####### Add some Labels for personal data
        for i in self.information_labels:
            if i == "date_label":
                setattr(self.widget, i,  QLabel(QDateTime.currentDateTime().toString(),self))
            else:
                setattr(self.widget, i,  QLabel("_____________________________________________________",self))
            
            self.vbox_patients_info.addWidget(getattr(self.widget, i))
            getattr(self.widget, i).setFont(label_font)
            self.information_labels_objects[i] = getattr(self.widget, i)
        
        ####### Add some Entry edit boxes for entering new patients
        for  i in self.edit_fields:
            if i == "date_line_edit":
                setattr(self.widget, i,QDateTimeEdit())
                getattr(self.widget, i).setDateTime(QDateTime.currentDateTime())
                getattr(self.widget, i).setStyleSheet("padding :7px")
            elif i == "patient_id_line_edit":
                setattr(self.widget, i,  QLabel("_",self))
                getattr(self.widget, i).setStyleSheet("padding :7px")
            else:
                setattr(self.widget, i, QLineEdit())
                getattr(self.widget, i).setStyleSheet("padding :7px")
            self.vbox_patients_info.addWidget(getattr(self.widget, i))
            getattr(self.widget, i).hide()
            self.edit_fields_objects[i] = getattr(self.widget, i)

        ####### Add some buttons for editing the informations fields
        edit_buttons_dico_mainwindow = {"done_edit_button":["ok_logo2.png",self.controller.add_new_patient_done_controller],
                                        "add_patient_button" : ["add_logo.png",self.controller.add_new_patient_controller ],
                                        "done_update_button" : ["ok_logo2.png", self.controller.update_patient_done_controller],
                                        "update_button" : ["edit_logo.png",self.controller.update_patient_controller],
                                        "cancel_action_button" : ["cancel_logo.png",self.controller.cancel_action_controller],
                                        "delete_patient_button" : ["delete_logo.png", self.controller.delete_patient_controller],
                                        "next_patient_button" : ["next_arrow.png", self.controller.show_next_patient_controller],
                                        "previous_patient_button" : ["previous_arrow.png", self.controller.show_previous_patient_controller]
                                        }
                                        # FORMAT --> BUTTON NAME: ICON IMAGE, FUNCTION ASOCIATED
        for i,j in edit_buttons_dico_mainwindow.items():
            setattr(self.widget, i, QPushButton('', self))
            temporal_button_pointer  = getattr(self.widget, i)
            temporal_button_pointer.setIcon(QtGui.QIcon(controller.resource_path(j[0])))
            temporal_button_pointer.setIconSize(QSize(20, 20))
            temporal_button_pointer.resize(12,12)
            temporal_button_pointer.clicked.connect(j[1])
            self.vbox_patients_info_editing.addWidget(temporal_button_pointer)
            if i in ["done_edit_button", "done_update_button", "cancel_action_button"]:
                temporal_button_pointer.hide()
            if i in ["delete_patient_button", "update_button","next_patient_button","previous_patient_button"]:
                temporal_button_pointer.setEnabled(False)

        ####### add some  action buttons to placeholder widget
        action_buttons_dico_mainwindow = {"startbutton":['Start',"start_logo2.png",controller.start],
                                            "stopbutton":['Stop',"pause_logo.png", controller.stop],
                                            "snapbutton":['Snapshot',"snapshot_logo.png",controller.snap],
                                            "startsavebutton":['Start Recording',"record_logo.png",controller.start_saving],
                                            "stopsavebutton":['Stop Recording',"stop_record.png",controller.stop_saving],
                                            "resetbutton":['Reset',"reset_logo.png",controller.reset_canvas]
                                            }
                                            # FORMAT --> BUTTON NAME :  TEXT IN THE BUTTON, ICON IMAGE, FUNCTION ASOCIATED
        for i,j in action_buttons_dico_mainwindow.items():
            setattr( self.widget, i,QPushButton(j[0], self))
            temporal_button_pointer = getattr( self.widget, i)
            temporal_button_pointer.setIcon(QtGui.QIcon(controller.resource_path(j[1])))
            temporal_button_pointer.clicked.connect(j[2])
            self.vbox_buttons.addWidget(temporal_button_pointer)
            if i != "startbutton":
                temporal_button_pointer.setEnabled(False)
        ####### add Spinboxes, labels and buttons for canvas ax1 and ax2 display control to 
        #placeholder widget and add some buttons to placeholder Event manager widget
        plot_buttons_dico_mainwindow = {"doubleSpinBox_pressure1": [None ,0,1000, 0.0, controller.recover_result_pressure_spinbox, None, self.vbox_pressure_range_canvas1],
                                        "auto_pressure_button_ax1": ['Auto',None,None,None,controller.auto_range_canvas_pressure,'font: bold 10px',self.vbox_pressure_range_canvas1],
                                        "doubleSpinBox_pressure2": [None,0,1000, 0.0,controller.recover_result_pressure_spinbox, None,self.vbox_pressure_range_canvas2],
                                        "auto_pressure_button_ax2": ['Auto',None,None,None,controller.auto_range_canvas_pressure, 'font: bold 10px' ,self.vbox_pressure_range_canvas2],
                                        "doubleSpinBox_time2": [ None, 5,500,10.0,controller.recover_result_time_spinbox, None ,self.vbox_time_range_canvas2],
                                        "default_time_button": ['Default',None,None,None, controller.default_range_canvas_time, 'font: bold 10px',self.vbox_time_range_canvas2],
                                        "combobox_time": [["0","5","10","15","20"],self.vbox_event_manager],
                                        "markerbutton": ['mark',"mark_logo.png", controller.start_marking,self.vbox_event_manager]
                                        }
                                        # FORMAT --> TEXT IN THE BUTTON, MIN RANGE, MAX RANGE, DEFAULT RANGE, FUNCTION ASSOCIATED, APPEAREANCE SETTINGS, VBOX ASOCIATED
                                        # FORMAT[combobox_time] --> VALUES IN INTE COMBOBOX, VBOX ASSOCIATED
                                        # FORMAT[markerbutton] --> TEXT, IN THE BUTTON, ICON IMAGE, FUNCTION ASOCIATED, VBOX ASOCIATED
        for i , j in plot_buttons_dico_mainwindow.items():
            if i in ["doubleSpinBox_pressure1",  "doubleSpinBox_pressure2","doubleSpinBox_time2"]:
                setattr(self.widget, i, QDoubleSpinBox(self))
                temporal_spinbox_pointer = getattr(self.widget, i)
                temporal_spinbox_pointer.setRange(j[1],j[2])
                temporal_spinbox_pointer.setValue(j[3])
                temporal_spinbox_pointer.valueChanged.connect(j[4])
                j[6].addWidget(temporal_spinbox_pointer)
                temporal_spinbox_pointer.setEnabled(False)
            if i in ["auto_pressure_button_ax1", "auto_pressure_button_ax2", "default_time_button"]:
                setattr(self.widget, i, QPushButton(j[0], self))
                temporal_button_pointer = getattr(self.widget, i)
                temporal_button_pointer.setStyleSheet(j[5])
                temporal_button_pointer.clicked.connect(j[4])
                j[6].addWidget(temporal_button_pointer)
                temporal_button_pointer.setEnabled(False)
            if i == "combobox_time":
                setattr(self.widget, i, QComboBox(self))
                temporal_combobox_pointer = getattr(self.widget, i)
                temporal_combobox_pointer.addItems(j[0])
                j[1].addWidget(temporal_combobox_pointer)
                temporal_combobox_pointer.setEnabled(False)
            if i ==  "markerbutton":
                setattr(self.widget, i, QPushButton(j[0], self))
                temporal_button_pointer = getattr(self.widget, i)
                temporal_button_pointer.setIcon(QtGui.QIcon(controller.resource_path(j[1])))
                temporal_button_pointer.clicked.connect(j[2])
                j[3].addWidget(temporal_button_pointer)
                temporal_button_pointer.setEnabled(False)
        self.snaps_buttons_dico_mainwindow = {"ax3": [False, None],
                                        "ax4": [False, None],
                                        "ax5": [False, None],
                                        "ax6": [False, None],
                                        "ax7": [False, None],
                                        "ax8": [False, None],
                                        "open_snaps":['Open Snaps',None, controller.open_selected_snaps, 'font: bold 10px',self.vbox_snaps_manager]}
                                        # FORMAT --> AX checkbox name : Status and pointer to the object
                                        # FORMAT --> button name : text in the button, pointer to the object, function associated, display parammeters and vbox name
        for i , j in  self.snaps_buttons_dico_mainwindow.items():
            if i == "open_snaps":
                setattr(self.widget, i, QPushButton(j[0], self))
                temporal_button_pointer = getattr(self.widget, i)
                temporal_button_pointer.setStyleSheet(j[3])
                temporal_button_pointer.clicked.connect(j[2])
                self.vbox_snaps_manager.addWidget(temporal_button_pointer)
                temporal_button_pointer.setEnabled(False)
                j[1] = temporal_button_pointer
            else: 
                setattr(self.widget, i, QCheckBox(i, self))
                temporal_checkbox_pointer = getattr(self.widget, i)
                temporal_checkbox_pointer.stateChanged.connect(partial(controller.select_snaps_controller, i))
                self.vbox_snaps_manager.addWidget(temporal_checkbox_pointer)
                temporal_checkbox_pointer.setEnabled(False)
                j[1] = temporal_checkbox_pointer
        self.show()  ### show everything

    
    ####### Functions for manage the plot data display
    def refresh_plot_view_mainwindow(self, pressure_range1,pressure_range2, time_window):
        """ Refresh the plot every time thi function is called by def model.complete_measurement_repetition
        --> controller.refresh_plot_controller--> 
        view.refresh_plot_view_mainwindow  """
        ###### ax1 plot eveything
        self.screen_canvas.ax1.cla() 
        # CLA() deletes the values in the plot without deleting the plot
        self.screen_canvas.ax2.cla()
        self.screen_canvas.ax1.yaxis.grid(color='gray', linestyle='dashed') 
        # draw dash lines in the background of the plot for better data visualization
        self.screen_canvas.ax1.xaxis.grid(color='gray', linestyle='dashed')
        self.screen_canvas.ax1.set_ylabel('Pressure')
        self.controller.markers_on_plot()
        # evaluates if the markerbool values is true to start marking the new added values in the plot
        self.screen_canvas.ax1.plot(np.arange(0,len(self.controller.model.values_save_p1))/170,self.controller.model.values_save_p1, color = 'b', label = 'sensor 1', marker = '*', markevery = self.controller.plot1_markers)
        self.screen_canvas.ax1.plot(np.arange(0,len(self.controller.model.values_save_p2))/170,self.controller.model.values_save_p2, color = 'r',  label = 'sensor 2',marker = '*', markevery = self.controller.plot1_markers)
        self.screen_canvas.ax1.legend(loc = 1)
        self.screen_canvas.ax1.set_xlim([0,len(self.controller.model.values_save_p1)/170])
        if pressure_range1 != 0: 
            # evaluates if there is a change in the range values entered by the user to adapt 
            # the plot axes values
            self.screen_canvas.ax1.set_ylim([-pressure_range1,pressure_range1])
        else:
            self.screen_canvas.ax1.set_ylim(auto=True)
        self.setup_slider() # calls the function to adapt the view of the first plot to the position 
                            #of the scroll bar values
        ###### ax2 plot for the defined time window (default 10 sec)
        time_window = self.controller.time_window
        self.screen_canvas.ax2.set_axisbelow(True)
        self.screen_canvas.ax2.plot(np.arange(int((170 * time_window )))/170,self.controller.model.values_save_p1_ax2, color = 'b', markevery = self.controller.plot1_markers)
        self.screen_canvas.ax2.plot(np.arange(int((170 * time_window )))/170,self.controller.model.values_save_p2_ax2, color = 'r', markevery = self.controller.plot1_markers)
        
        self.screen_canvas.ax2.yaxis.grid(color='gray', linestyle='dashed') 
        # draw dash lines in the background of the plot for better data visualization
        self.screen_canvas.ax2.xaxis.grid(color='gray', linestyle='dashed')
        self.screen_canvas.ax2.set_ylabel('Pressure')
        self.screen_canvas.ax2.set_xlabel('Time (s)')
        self.screen_canvas.ax2.set_xlim([0,time_window])
        if pressure_range2 != 0:
            self.screen_canvas.ax2.set_ylim([-pressure_range2,pressure_range2]) 
            # evaluates if there is a change in the range values entered by the user to 
            # adapt the plot axes values
        else:
            self.screen_canvas.ax2.set_ylim(auto=True)
        self.screen_canvas.draw()

    def snapshot(self, pressure_range2, time_window):
        """ evaluates of the snapbool value is TRUE, and creates a minisnap image at the 
        bottom of the canvas (it can create a maximum of 6) """
        self.controller.snap_dico_checking(self.controller.snap_canvas_number) 
        # function that checks when all the minisnaps plots are occupied, and once is done, 
        # it starts to replace the old plots wht the new, from ax3 to ax8  
        for canvas_id, status in self.controller.snap_canvas_number.items():
                if status[0] is False: #evaluates which position is available for plot a new minisnap
                    if status[4] is not None: # evaluates which position is empty 
                        self.controller.snap_canvas_number[canvas_id][4].remove() 
                        # if there is an pre-existing plot in that postion, it delete it
                    self.controller.snap_canvas_number[canvas_id][4] = self.screen_canvas.fig.add_subplot(status[1],status[2],status[3])
                    self.controller.snap_canvas_number[canvas_id][4].set_xlim([0,time_window]) 
                    self.controller.snap_canvas_number[canvas_id][4].yaxis.grid(color='gray', linestyle='dashed')
                    self.controller.snap_canvas_number[canvas_id][4].xaxis.grid(color='gray', linestyle='dashed')
                    time_window_copy = self.controller.time_window 
                    # creates a copy of the time_window variable
                    p1_copy = self.controller.model.values_save_p1_ax2.copy() 
                    # creates a copy of all the data used for create the plot
                    p2_copy = self.controller.model.values_save_p2_ax2.copy()
                    self.controller.snap_canvas_number[canvas_id][4].plot(np.arange(int((170 * time_window )))/170,self.controller.model.values_save_p1_ax2, color = 'b')
                    self.controller.snap_canvas_number[canvas_id][4].plot(np.arange(int((170 * time_window )))/170,self.controller.model.values_save_p2_ax2, color = 'r')
                    
                    self.controller.snap_canvas_number[canvas_id][4].title.set_text(canvas_id)
                    self.controller.snap_canvas_number[canvas_id][4].set_ylabel('Pressure', fontsize = 6)
                    self.controller.snap_canvas_number[canvas_id][4].set_xlabel('Time (s)', fontsize = 6)
                    self.controller.snap_canvas_number[canvas_id][5] = [p1_copy,p2_copy,time_window_copy,canvas_id] # insert the copy of the values in the plots dico to be used in case the minisnap s clicked an open in the second window
                    self.controller.snap_canvas_number[canvas_id][0] = True 
                    # changes the status in the canvas dico as active.
                    self.controller.enable_buttons(self.snaps_buttons_dico_mainwindow[canvas_id][1])
                    self.snaps_buttons_dico_mainwindow[canvas_id][0] = True
                    self.snaps_buttons_dico_mainwindow[canvas_id][1].setChecked(False)
                    self.controller.enable_disable_snap_button()  
                    
                    if pressure_range2 != 0:
                        self.controller.snap_canvas_number[canvas_id][4].set_ylim([pressure_range2,-pressure_range2])
                        # evaluates if there is a change in the range values entered by the user to adapt 
                        # the plot axes values
                    else:
                        self.controller.snap_canvas_number[canvas_id][4].set_ylim(auto=True)
                    self.controller.snapbool =  False
                    break
        self.screen_canvas.draw()
    
    def setup_slider(self):
        """ Function that uptade the limits of the x axis in plot ax1, and after 
        that calls the function update  """
        self.lims = np.array(self.screen_canvas.ax1.get_xlim())
        self.update_slider_mainwindow_view()

    def update_slider_mainwindow_view(self):
        """according to the position of the scroll bar, it sets the limits of the x
         axis to show only a part of the ax1 plot  """
        scrollbar_value = self.scroll_bar.value() / ((1 + self.scroll_bar_step) * 100)
        l1 = self.lims[0] + scrollbar_value * np.diff(self.lims)
        l2 = l1 + np.diff(self.lims) * self.scroll_bar_step
        self.screen_canvas.ax1.set_xlim(l1,l2)
        self.screen_canvas.draw_idle()

    def clear_canvas_view(self):
        """ Specific funtion for clear all the plots and the snapshots """
        self.screen_canvas.ax1.cla()
        self.screen_canvas.ax2.cla()
        for i in self.controller.snap_canvas_number.keys():
                if self.controller.snap_canvas_number[i][4] is not None:
                    self.controller.snap_canvas_number[i][4].remove()
                    self.controller.snap_canvas_number[i][4] = None
                    self.controller.snap_canvas_number[i][0] = False
        self.screen_canvas.ax1.yaxis.grid(color='gray', linestyle='dashed')
        self.screen_canvas.ax1.xaxis.grid(color='gray', linestyle='dashed')
        self.screen_canvas.ax2.yaxis.grid(color='gray', linestyle='dashed')
        self.screen_canvas.ax2.xaxis.grid(color='gray', linestyle='dashed')
        self.screen_canvas.ax1.set_ylabel('Pressure')
        self.screen_canvas.ax2.set_ylabel('Pressure')
        self.screen_canvas.ax2.set_xlabel('Time (s)')
        self.screen_canvas.draw()
    
    ####### Functions for managing the data loading and saving
    def save_filedialog_mainwindow(self):
        """ Creates a Dialog window for the user to choose a file and 
        a location and returns the name of the file (with the path) for saving porpouses """

        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;Text Files (*.txt)", options=options)
        if fileName:
            return fileName

    def load_filedialog_mainwindow(self):
        """ Creates a Dialog window for the user to choose a file and a location and 
        returns the name of the file (with the path) for loading porpouses """
        file_name = QFileDialog.getOpenFileName()
        if file_name:
            return file_name

    def saving_file_type_view(self):
        """ Creates a Dialog window for the user to choose an exporting format """
        file_types = [".csv", ".mat" ,".txt"]
        selected_file_type, ok = QInputDialog.getItem(self, "Select file type",  "List of availables file types", file_types, 0, False)
        if ok == False:
            return None
        else:
            return selected_file_type

    def open_file_name_dialog_mainwindow(self):
        """ Creates a Dialog window for the user to choose a file and a location and returns the name of 
        the file (with the path) for database loading porpouses.
        if there is no selection by the user, it returns a default database name """
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
        if fileName:
            return fileName
        else:
            return ":memory:" #WORKING IN THE MEMORY OPTION

    ####### Functions for managing the database and patients info display
    def add_new_patient_view(self):
        """ Function that changes the GUI when the add_new_patient button is 
        pressed. it show the Input widgets fot he user to enter the 
        information of the new patient and deactivate the respective buttons 
        for the user be able to run again the programm once the new patient
        is added"""
        self.controller.disable_buttons(self.widget.startbutton,self.widget.delete_patient_button,
                                        self.widget.next_patient_button,self.widget.previous_patient_button )
        self.widget.done_edit_button.show()
        self.widget.cancel_action_button.show()
        self.widget.add_patient_button.hide()
        self.widget.update_button.hide()
        for i in self.information_labels_objects.values():
            i.hide()
        for  j in self.edit_fields_objects.values():
            if isinstance(j,QLabel):
                j.setText("_")
            j.show()
    
    def update_patient_view(self):
        """ Function that changes the GUI when the update_patient button is pressed.
        it show the Input widgets fot he user to update the informationof the patient 
        and deactivate the respective buttons for the user be able to run again 
        the programm once the new patient is added"""

        self.controller.disable_buttons(self.widget.startbutton,self.widget.delete_patient_button,
                                        self.widget.next_patient_button,self.widget.previous_patient_button)
        self.widget.done_update_button.show()
        self.widget.cancel_action_button.show()
        self.widget.update_button.hide()
        self.widget.add_patient_button.hide()
        for (k,v), (k2,v2) in zip(self.edit_fields_objects.items(), self.information_labels_objects.items()):
            v2.hide()
            v.show()
            if k == "date_line_edit":
                v.setDateTime(QDateTime.fromString(v2.text()))
            else:    
                v.setText(v2.text())

    def add_new_patient_done_view(self):
        """  once the user have finish to enter the information for the new patient, it 
        sets the GUI with the new data and enables the user to continue using the other 
        features of the programm """
        for (k,v), (k2,v2) in zip(self.edit_fields_objects.items(), self.information_labels_objects.items()):
            if k2 == "patient_id_label":
                v2.setText(str(self.controller.model.cursor.lastrowid))
            else:
                v2.setText(str(v.text()))
            v2.show()
        for  j in self.edit_fields_objects.values():
            j.hide()
            if isinstance(j,QDateTimeEdit):
                j.setDateTime(QDateTime.currentDateTime())
            else:
                j.setText("")
        self.widget.done_edit_button.hide()
        self.widget.cancel_action_button.hide()
        self.widget.add_patient_button.show()
        self.widget.update_button.show()
        self.controller.enable_buttons(self.widget.startbutton,
                                        self.widget.update_button,
                                        self.widget.delete_patient_button,
                                        self.widget.next_patient_button,
                                        self.widget.previous_patient_button
                                        )
    
    def cancel_action_view(self):
        """ Functions that sets the GUI to the previous visual state in case the 
        users regrets to modifiy the database"""

        self.widget.done_edit_button.hide()
        self.widget.done_update_button.hide()
        self.widget.cancel_action_button.hide()
        self.widget.add_patient_button.show()
        self.widget.update_button.show()
        self.controller.enable_buttons(self.widget.startbutton,
                                        self.widget.update_button,
                                        self.widget.delete_patient_button,
                                        self.widget.next_patient_button,
                                        self.widget.previous_patient_button 
                                        )
        for (k,v), (k2,v2) in zip(self.edit_fields_objects.items(), self.information_labels_objects.items()):
            v2.show()
            v.hide()
            if isinstance(v,QDateTimeEdit):
                v.setDateTime(QDateTime.currentDateTime())
            else:
                v.setText("")

    def update_patient_done_view(self):
        """  once the user have finish to update the information for the patient,
        it sets the GUI with the new data and enables the user to continue using 
        the other features of the programm """
        
        for (k,v), (k2,v2) in zip(self.edit_fields_objects.items(), self.information_labels_objects.items()):
            v2.setText(str(v.text()))
            v2.show()
        for  j in self.edit_fields_objects.values():
            j.hide()
            if isinstance(j,QDateTimeEdit):
                j.setDateTime(QDateTime.currentDateTime())
            else:
                j.setText("")   
        self.widget.done_update_button.hide()
        self.widget.update_button.show()
        self.controller.enable_buttons(self.widget.startbutton,
                                        self.widget.add_patient_button,
                                        self.widget.delete_patient_button,
                                        self.widget.next_patient_button,
                                        self.widget.previous_patient_button
                                        )
    
    def delete_patient_done_view(self):
        """ it detes the information from the already deleted from the data
         base patient from the GUI"""
        for i in self.information_labels_objects.keys():
            if i == "date_label":
                self.information_labels_objects[i].setText(QDateTime.currentDateTime().toString())
            else:
                self.information_labels_objects[i].setText("_____________________________________________________")
        self.controller.disable_buttons(self.widget.delete_patient_button,self.widget.update_button)

    ####### Function for manage the different error messages for the user
    def error_message(self, controller, alert):
        """ Function for manage the different error messages for the user"""
        if alert == "no device":
            controller.msg = QMessageBox()
            controller.msg.setWindowTitle("Arduino device not connected")
            controller.msg.setText("Dataloger pression not connected")
            controller.msg.setIcon(QMessageBox.Critical)
            controller.msg.setStandardButtons(QMessageBox.Retry)
            controller.msg.setStandardButtons(QMessageBox.Close)
            controller.x = controller.msg.exec_()
        elif alert == "no database":
            controller.msg = QMessageBox()
            controller.msg.setWindowTitle("Incorrect database file selected")
            controller.msg.setText("The selected file is not a database type")
            controller.msg.setIcon(QMessageBox.Critical)
            controller.msg.setStandardButtons(QMessageBox.Retry)
            x = controller.msg.exec_()
        elif alert == "new entry empty field(s)":
            controller.msg = QMessageBox()
            controller.msg.setWindowTitle("Empty fields")
            controller.msg.setText("All fields in patients must be fill")
            controller.msg.setIcon(QMessageBox.Critical)
            controller.msg.setStandardButtons(QMessageBox.Retry)
            controller.x = controller.msg.exec_()
    
    ####### Functions for managing inputs from the user
    def users_port_selection(self, items):
        """ It shows the user a list with all the available ports rs232, and returns 
        the selection and the status of the question (if he aswer or not) """
        item, ok = QInputDialog.getItem(self, "Select pression capture device",  "List of availables ports", items, 0, False)
        return  item, ok
    
    def data_load_question_view(self):
        """ Shows a input file dialagog box, to ask the user if he wants
        to load a pre-existing database  """
        button_response = QMessageBox.question(self,"Load Database", "Would you like to load an existing database", 
        buttons= QMessageBox.Ok | QMessageBox.No, defaultButton=QMessageBox.No,)
        return button_response
    
    def new_database_creation_view(self):
        """ Shows a input dialagog box, to ask the user for the name of the new database  """
        database_name, ok = QInputDialog.getText(self, 'Database', 'Enter the name of the new Database: ')
        return database_name, ok
    
    def set_snap_title_view(self):
        """ Shows a input dialagog box, to ask the user for the title of the snap  """
        snap_title, ok = QInputDialog.getText(self, 'Title', 'Enter the title of the selected snapshot: ')
        return snap_title, ok
    
    def delete_patient_view_question(self):
        """ Shows a input dialagog box, to ask the user if he is sire of deleting
        the current patient from the database """
        button_response = QMessageBox.question(self,"Delete patient", "Are you sure you want to delete this patient and all his experiences from the database?", 
        buttons=QMessageBox.Ok | QMessageBox.No, defaultButton=QMessageBox.No,)
        return button_response
    
        

class SecondWindow(QMainWindow):
    """ Class for create the SecondWindow object for displaying the QTPY widgets 
    and canvas for the user to interact with. This window specifically 
    display the clicked snapshot minisnaps from the mainwindow, and give 
    the user the options of saving and visualization """
    def __init__(self, controller, *args, **kwargs):
        super(SecondWindow, self).__init__(*args, **kwargs)
        self.controller = controller
        plt.ion()
        self.setMinimumWidth(500)
        self.setMinimumHeight(400)
        self.setWindowTitle("Programme d'acquisition de la pression (LNC Laboratoire de neurosciences cognitives UMR 7291)")
        self.showMaximized()
        #creates a canvas object
        self.screen_canvas2 = MplCanvas_secondwindow(self ,controller, 12, 5, 100)
        #creates a tollbar object
        self.toolbar = MyToolbar(self.screen_canvas2,self)
        layout = QGridLayout()
        layout.addWidget(self.toolbar, 1,1,1,4)
        layout.addWidget(self.screen_canvas2,2,2,6,10)
        # Create a placeholder widget to hold our toolbar and the buttons
        self.widget = QWidget()
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)
        count = 2
        for i in self.screen_canvas2.axes_list:
                self.groupbox_saving_options = QGroupBox(self)
                self.groupbox_saving_options.setTitle('Plots Manager')
                self.vbox_saving_options = QVBoxLayout(self.groupbox_saving_options)
                self.groupbox_saving_options.setLayout(self.vbox_saving_options)
                layout.addWidget(self.groupbox_saving_options,count,1)
         
                temp_ax = getattr(self.screen_canvas2.fig, i[0])
                self.widget.savebutton = QPushButton('', self)
                self.widget.savebutton.move(400,15)
                self.widget.savebutton.setIcon(QtGui.QIcon(controller.resource_path("save_logo.png")))
                self.widget.savebutton.clicked.connect(partial(controller.save_subplot_secondwindow_controller, self, i[0]))
                self.vbox_saving_options.setEnabled(True)
                self.vbox_saving_options.addWidget(self.widget.savebutton)

                self.widget.closebutton = QPushButton('', self)
                self.widget.closebutton.move(400,15)
                self.widget.closebutton.setIcon(QtGui.QIcon(controller.resource_path("close_logo.png")))
                self.widget.closebutton.clicked.connect(partial(controller.delete_subplot_secondwindow_controller, i[0]))
                self.vbox_saving_options.setEnabled(True)
                self.vbox_saving_options.addWidget(self.widget.closebutton)
                # This part of the code adapts the distancing of the plot manager box to fit the number of displayed plots
                if len(self.screen_canvas2.axes_list) == 1:
                    count +=1
                elif len(self.screen_canvas2.axes_list) == 2:
                    count +=2
                elif len(self.screen_canvas2.axes_list) == 3:
                    count +=2
                elif len(self.screen_canvas2.axes_list) >= 4:
                    count +=1
        self.groupbox_export_options = QGroupBox(self)
        self.groupbox_export_options.setTitle('Export Manager')
        self.vbox_export_options = QVBoxLayout(self.groupbox_export_options)
        self.groupbox_export_options.setLayout(self.vbox_export_options)
        if count > 4:
            layout.addWidget(self.groupbox_export_options,count,1)
        else:
            layout.addWidget(self.groupbox_export_options,count+1,1) 
        self.widget.setLayout(layout)
        self.widget.export_button = QPushButton('Create Report', self)
        self.widget.export_button.move(400,15)
        self.widget.export_button.clicked.connect(controller.create_report_secondwindow_controller)
        self.vbox_export_options.setEnabled(True)
        self.vbox_export_options.addWidget(self.widget.export_button)

        self.widget.export_data_button = QPushButton('Export data', self)
        self.widget.export_data_button.move(400,15)
        self.widget.export_data_button.clicked.connect(controller.export_data_secondwindow_controller)
        self.vbox_export_options.setEnabled(True)
        self.vbox_export_options.addWidget(self.widget.export_data_button)
    
    def saveFileDialog_secondwindow(self):
        """ Display a dialog window for the user to choose the name and directory
        of the report to be saved"""
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;Text Files (*.txt)", options=options)
        if fileName:
            return fileName
    
    def saving_file_type_view(self):
        """ Creates a Dialog window for the user to choose an exporting format """
        file_types = [".csv", ".jpg"]
        selected_file_type, ok = QInputDialog.getItem(self, "Select file type",  "List of availables file types", file_types, 0, False)
        if ok == False:
            return None
        else:
            return selected_file_type
    
    def observation_input_dialog_secondwindow(self):
        """ Shows a input dialagog box, to ask the user for entering obervations into the report"""
        observations, ok = QInputDialog.getMultiLineText(self, 'Observations', 'Enter observations : ')
        return observations, ok

class ThirdWindow(QMainWindow):
    """ Class for create the ThirdWindow object for displaying the QTPY widgets and canvas 
    for the user to interact with. This window specifically display the data loaded by the user """
    def __init__(self, controller, *args, **kwargs):
        super(ThirdWindow, self).__init__(*args, **kwargs)
        self.controller = controller
        #dictionaries with the diferent informations fields to be displayed at the third window
        self.information_labels_case = [["doctor_name_label_case","Doctors Name :"],
                                        ["patient_name_label_case","Patients Name :"],
                                        ["patient_id_label_case","Patients ID :"],
                                        ["date_label_case","Date :"],
                                        ["city_label_case","City :"]
                                        ]
        self.information_labels = ["doctor_name_label","patient_name_label",
                                    "patient_id_label","date_label","city_label"
                                    ]
        self.information_labels_objects = {}
        self.information_labels_case_objects = {}
        plt.ion()
        self.setMinimumWidth(500)
        self.setMinimumHeight(400)
        self.setWindowTitle("Programme d'acquisition de la pression (LNC Laboratoire de neurosciences cognitives UMR 7291)")
        self.showMaximized()
        # canvas object creation for the third window 
        self.screen_canvas3 = MplCanvas_Thirdwindow(self ,controller, 12, 5, 100)
        #######Create Scrollbar to move through the fist plot
        self.scroll_bar = QScrollBar(QtCore.Qt.Horizontal)
        self.scroll_bar.setValue(99)
        self.scroll_bar.setEnabled(True)
        self.scroll_bar_step = .1
        self.scroll_bar.setPageStep(self.scroll_bar_step * 100)
        self.scroll_bar.sliderReleased.connect(self.update_slider_thirdwindow_view)
        self.setup_slider()
        ####### Creates the Groupboxes for reagrupe widgets with different functions
        self.groupbox_dico_mplcanva_thirdwindow = {
                                        "groupbox_patients_info_case" : ["vbox_patients_info_case", "QGroupBox { background-color: rgb(224, 224, 224) }", 'Fields', None],
                                        "groupbox_patients_info" : ["vbox_patients_info",None,'Personal data'],        
                                        "groupbox_pressure_range_canvas2" : ["vbox_pressure_range_canvas2" ,None , 'Set Pressure range'],
                                        "groupbox_time_range_canvas2" : ["vbox_time_range_canvas2" ,None ,'Set time range'],
                                                }
                                        #FORMAT --> GROUPBOX NAME : VBOX NAME, APPEAREANCE SETTINGS, BOX TITLE 
        for i, j in  self.groupbox_dico_mplcanva_thirdwindow.items():
            setattr(self, i, QGroupBox(self))
            setattr(self, j[0],QVBoxLayout(getattr(self, i)))
            if i == "groupbox_patients_info_case": 
                getattr(self, i).setStyleSheet(j[1])
            if i in ["groupbox_patients_info_case","groupbox_patients_info", "groupbox_pressure_range_canvas2", "groupbox_event_manager"]:
                getattr(self, i).setTitle(j[2])
            if i in ["groupbox_patients_info_case","groupbox_patients_info" ]:
                getattr(self, j[0]).setSpacing(0)
            getattr(self, i).setLayout(getattr(self, j[0]))
        ####### Layout creation for placing the different widgets in the mainwindow
        layout_dico_mainwindow = {
                                "groupbox_patients_info_case" : [0,2,1,2],
                                "groupbox_patients_info": [0,4,1,6],
                                "scroll_bar": [1,1,1,11],
                                "groupbox_pressure_range_canvas2": [2,0,1,1],
                                "groupbox_time_range_canvas2": [3,0,1,1],
                                "screen_canvas3" : [2,1,7,11]
                                }
                                # FORMAT --> OBJECT NAME :  ROW, COLUMN, NUMBER OF ROW USED, NUMBER OF COLUMN USED 
        layout = QGridLayout()
        for i , j in layout_dico_mainwindow.items():
            layout.addWidget(getattr(self,i), j[0],j[1],j[2],j[3])
        ####### Create a placeholder widget to hold our buttons and so on
        self.widget = QWidget()
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)
        ####### Add some Labels for identify patient info and doctor
        label_font = QtGui.QFont('Arial', 10)
        label_font.setBold(True)
        for i in self.information_labels_case:
            setattr(self.widget, i[0],  QLabel(i[1],self))
            self.vbox_patients_info_case.addWidget(getattr(self.widget, i[0]))
            getattr(self.widget, i[0]).setFont(label_font)
            self.information_labels_case_objects[i[0]] = getattr(self.widget, i[0])
        ####### Add some Labels for personal data
        for i, j in zip(self.controller.model.patient_load_info, self.information_labels):
            setattr(self.widget, j,  QLabel(str(i),self))
            self.vbox_patients_info.addWidget(getattr(self.widget, j))
            getattr(self.widget, j).setFont(label_font)
            self.information_labels_objects[j] = getattr(self.widget, j)
        ####### add Spinboxes, labels and buttons for canvas ax1 display control to placeholder widget
        plot_buttons_dico_mainwindow = {"doubleSpinBox_pressure1": [None ,0,300, 0.0, controller.recover_result_pressure_spinbox, None, self.vbox_pressure_range_canvas2],
                                        "auto_pressure_button_ax1": ['Auto',None,None,None,controller.auto_range_canvas_pressure,'font: bold 10px',self.vbox_pressure_range_canvas2],
                                        "doubleSpinBox_time2": [ None, 5,500,10.0,controller.recover_result_time_spinbox, None ,self.vbox_time_range_canvas2],
                                        "default_time_button": ['Default',None,None,None, controller.default_range_canvas_time, 'font: bold 10px',self.vbox_time_range_canvas2],
                                        }
                                        # FORMAT --> TEXT IN THE BUTTON, MIN RANGE, MAX RANGE, DEFAULT RANGE, FUNCTION ASSOCIATED, APPEAREANCE SETTINGS, VBOX ASOCIATED
        for i , j in plot_buttons_dico_mainwindow.items():
            if i in ["doubleSpinBox_pressure1","doubleSpinBox_time2"]:
                setattr(self.widget, i, QDoubleSpinBox(self))
                temporal_spinbox_pointer = getattr(self.widget, i)
                temporal_spinbox_pointer.setRange(j[1],j[2])
                temporal_spinbox_pointer.setValue(j[3])
                temporal_spinbox_pointer.valueChanged.connect(j[4])
                j[6].addWidget(temporal_spinbox_pointer)
                temporal_spinbox_pointer.setEnabled(True)
            if i in ["auto_pressure_button_ax1", "default_time_button"]:
                setattr(self.widget, i, QPushButton(j[0], self))
                temporal_button_pointer = getattr(self.widget, i)
                temporal_button_pointer.setStyleSheet(j[5])
                temporal_button_pointer.clicked.connect(j[4])
                j[6].addWidget(temporal_button_pointer)
                temporal_button_pointer.setEnabled(True)
        self.refresh_plot()

    def refresh_plot(self):
        """ it fills the AX1 plot from the third window with the loaded information from the model"""
        self.screen_canvas3.ax1.cla()
        self.screen_canvas3.ax1.yaxis.grid(color='gray', linestyle='dashed')
        self.screen_canvas3.ax1.xaxis.grid(color='gray', linestyle='dashed')
        self.screen_canvas3.ax1.set_ylabel('Pressure')
        self.screen_canvas3.ax1.set_xlabel('Time (s)')
        self.screen_canvas3.ax1.plot(np.arange(0,len(self.controller.model.values_load_p1))/170,self.controller.model.values_load_p1, color = 'b', label = 'sensor 1', marker = '*', markevery = self.controller.plot1_markers)
        self.screen_canvas3.ax1.plot(np.arange(0,len(self.controller.model.values_load_p2))/170,self.controller.model.values_load_p2, color = 'r',  label = 'sensor 2',marker = '*', markevery = self.controller.plot1_markers)
        self.screen_canvas3.ax1.legend(loc = 1)
        self.screen_canvas3.ax1.set_xlim([0,len(self.controller.model.values_load_p1)/170])
        if self.controller.pressure_range_thirdwindow != 0:
            self.screen_canvas3.ax1.set_ylim([-self.controller.pressure_range_thirdwindow,self.controller.pressure_range_thirdwindow])
        else:
            self.screen_canvas3.ax1.set_ylim(auto=True)
        self.setup_slider()
    
    def setup_slider(self):
        """ Function that uptade the limits of the x axis in plot ax1, and after that 
        calls the function update  """
        self.lims = np.array(self.screen_canvas3.ax1.get_xlim())
        self.update_slider_thirdwindow_view()

    def update_slider_thirdwindow_view(self):
        """according to the position of the scroll bar, it sets the limits of the x 
        axis to show only a part of the ax1 plot  """
        scrollbar_value = self.scroll_bar.value() / ((1 + self.scroll_bar_step) * 100)
        l1 = self.lims[0] + scrollbar_value * np.diff(self.lims)
        l2 = l1 + np.diff(self.lims) * self.scroll_bar_step
        self.screen_canvas3.ax1.set_xlim((l1-(self.controller.time_window_thirdwindow/20)),(l2+(self.controller.time_window_thirdwindow/20)))
        self.screen_canvas3.draw_idle()

class PatientsExperiencesDatabase(QMainWindow):
    def __init__(self, controller,complete_database,columns_names,sender,*args, **kwargs ):
        super(PatientsExperiencesDatabase,self).__init__(*args, **kwargs)
        self.controller = controller
        self.database = complete_database
        self.columns_names = columns_names
        self.sender = sender
        self.title = 'Patients Database'
        self.left = 50
        self.top = 50
        self.width = 1000
        self.height = 1000
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.table = DatabaseTable(self, self.controller, self.database, self.columns_names)
        ####### Creates the Groupboxes for reagrupe widgets with different functions
        self.groupbox_dico_mplcanva_database = {
                                        "groupbox_search_patients" : ["vbox_search_patients", "QGroupBox { background-color: rgb(224, 224, 224) }", 'Search'],
                                        "groupbox_patients_buttons" : ["vbox_patients_buttons","QGroupBox { background-color: rgb(224, 224, 224) }",'Option']
                                                }
                                        #FORMAT --> GROUPBOX NAME : VBOX NAME, APPEAREANCE SETTINGS, BOX TITLE 
        for i, j in  self.groupbox_dico_mplcanva_database.items():
            setattr(self, i, QGroupBox(self))
            setattr(self, j[0],QHBoxLayout(getattr(self, i)))
            getattr(self, i).setStyleSheet(j[1])
            getattr(self, i).setTitle(j[2])
            getattr(self, j[0]).setSpacing(0)
            getattr(self, i).setLayout(getattr(self, j[0]))
        ####### Layout creation for placing the different widgets in the nwindow
        layout = QGridLayout()
        layout.addWidget(self.groupbox_search_patients,1,1)
        layout.addWidget(self.table.table_widget,2,1)
        layout.addWidget(self.groupbox_patients_buttons,3,1)
        ####### Create a placeholder widget to hold our buttons and so on
        self.widget = QWidget()
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)
        ####### Buttons and entries with different parametters and asociated action functions
        self.widget.search_bar = QLineEdit()
        self.widget.search_bar.setPlaceholderText("Search...")
        self.widget.search_bar.textChanged.connect(self.search)
        self.vbox_search_patients.addWidget(self.widget.search_bar)
        if self.sender == "patients_database_window":
            self.widget.set_patient = QPushButton('Set patient', self)
            self.widget.set_patient.clicked.connect(self.controller.set_current_patient_controller)
            self.vbox_patients_buttons.addWidget(self.widget.set_patient)

            self.widget.delete_patient = QPushButton('Delete patient', self)
            self.widget.delete_patient.clicked.connect(self.controller.delete_selected_patient_controller)
            self.vbox_patients_buttons.addWidget(self.widget.delete_patient)

        elif self.sender == "experiences_database_window":
            self.widget.load_experience = QPushButton('Load experience', self)
            self.widget.load_experience.clicked.connect(self.controller.open_selected_experience_controller)
            self.vbox_patients_buttons.addWidget(self.widget.load_experience)
            self.widget.delete_experience = QPushButton('Delete experience', self)
            self.widget.delete_experience.clicked.connect(self.controller.delete_selected_experience_controller)
            self.vbox_patients_buttons.addWidget(self.widget.delete_experience)

    def search(self, s):
        """Function triggered by the insertion of charachter at the input object 
        from the database window. it recover the current 
        string in the input and comparest it with all the values 
        from the Qtablewidget"""
        # Clear current selection.
        self.table.table_widget.setCurrentItem(None)
        if not s:
            # Empty string, don't search.
            return
        matching_items = self.table.table_widget.findItems(s, Qt.MatchContains)
        if matching_items:
            # We have found something.
            item = matching_items[0]  # Take the first.
            self.table.table_widget.setCurrentItem(item)

    def set_current_patient_view(self):
        """"This function is triggered when the button  self.widget.set_patient from
        the self.table_database_window object. This allow to set
        as the current patients the selected entry from the database, setting 
        all the values from the patients information box of the 
        mainwindow with the information from the selected patient. It also set all 
        the values to theirs base values to restart a new 
        recording session   """
        try:
            patient_info_list = []
            for j in range(0,len(self.table.table_widget.selectedIndexes())):
                index = self.table.table_widget.selectedIndexes()[j]
                patient_id = self.table.table_widget.model().data(index)
                patient_info_list.append(patient_id)
            patient_id = patient_info_list.pop(0)
            patient_info_list.insert(2,patient_id)
            self.close()
            return patient_info_list
        except IndexError:
            pass

    def delete_selected_patient_view(self):
        """ functions triggered by the functions def delete_selected_patient_controller() is 
        called, it performs the visualchanges in the GUI database window when a patient 
        is deleted"""
        try:
            index = self.table.table_widget.selectedIndexes()[0]
            patient_id = int(self.table.table_widget.model().data(index))
            return patient_id
        except IndexError:
            pass

    def open_selected_experience_view(self):
        """function that returns the values of the selected experience to be displayed
         in the third window"""
        try:
            index1 = self.table.table_widget.selectedIndexes()[0]
            experience_id = int(self.table.table_widget.model().data(index1))
            index2 = self.table.table_widget.selectedIndexes()[1]
            patients_id = int(self.table.table_widget.model().data(index2))
            self.close()
            return experience_id, patients_id
        except IndexError:
            return None, None


    def delete_selected_experience_view(self):
        """ functions triggered by the functions def delete_selected_experience_controller()
         is called, it performs the visual changes in the GUI database window when a
          patient is deleted"""
        try:    
            index1 = self.table.table_widget.selectedIndexes()[0]
            experience_id = int(self.table.table_widget.model().data(index1))
            return experience_id
        except IndexError:
            pass

class DatabaseTable(QWidget):
    def __init__(self,PatientsDatabase, controller,complete_database, columns_names, *args, **kwargs ):
        self.controller = controller
        self.database = complete_database
        self.columns_names = columns_names
        self.table_widget = QTableWidget()
        try: # Statement that manages the display of the empty table in case the  database is emty
            self.set_table_elements()
        except IndexError:
            pass

    def set_table_elements(self):
        """ FUnctions that set the table size according to the database number of entries"""
        #Row count
        self.table_widget.setRowCount(len(self.database)) 
        #Column count
        self.table_widget.setColumnCount(len(self.database[0]))  
        self.table_widget.setHorizontalHeaderLabels(self.columns_names)
        count1 = 0
        for i in self.database:
            count2 = 0
            for j in i:
                self.table_widget.setItem(count1,count2, QTableWidgetItem(str(i[count2])))
                count2 +=1
            count1 += 1
        #Table will fit the screen horizontally
        self.table_widget.horizontalHeader().setStretchLastSection(True)
        self.table_widget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.table_widget.setSelectionBehavior(QTableView.SelectRows)
        self.table_widget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table_widget.setSortingEnabled(True)
        super(DatabaseTable, self).__init__(self.table_widget)

